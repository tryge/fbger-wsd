package at.ac.univie.fbger.wsd.workflow;

import at.ac.univie.fbger.wsd.imp.WsdExtractor;
import at.ac.univie.fbger.wsd.imp.WsdImporter;
import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.InputStream;
import java.util.concurrent.Executor;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class WsdImportWorkflowTest {
	private Executor executor;
	private EventBus bus;
	private WsdExtractor extractor;
	private InputStream is;
	private WsdImporter importer;

	private WsdImportWorkflow workflow;

	@Before
	public void setUp() {
		executor = mock(Executor.class);
		bus = mock(EventBus.class);
		extractor = mock(WsdExtractor.class);
		is = mock(InputStream.class);
		importer = mock(WsdImporter.class);

		workflow = new WsdImportWorkflow(executor, extractor, importer, bus);
	}

	@Test
	public void testWsdImport() throws InterruptedException {
		workflow.execute(is);

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		Runnable runnable = captor.getValue();
		runnable.run();

		verify(extractor).read(same(is));
		verify(importer).awaitFinished();
		verify(bus).post("WSD");
	}

	@Test
	public void testWsdImportFailure() {
		workflow.execute(is);

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		Runnable runnable = captor.getValue();

		RuntimeException exception = mock(RuntimeException.class);
		doThrow(exception).when(extractor).read(same(is));

		runnable.run();

		verify(bus).post(exception);
	}
}
