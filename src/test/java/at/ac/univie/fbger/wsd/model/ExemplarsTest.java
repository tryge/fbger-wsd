package at.ac.univie.fbger.wsd.model;

import org.junit.Test;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

/**
 * @author michael.zehender@me.com
 */
public class ExemplarsTest {
	@Test
	public void givenExemplarsVisitThem() {
		ExemplarVisitor visitor = mock(ExemplarVisitor.class);
		Exemplar exemplar1 = mock(Exemplar.class);
		Exemplar exemplar2 = mock(Exemplar.class);

		Exemplars exemplars = new Exemplars(
			asList(exemplar1, exemplar2)
		);
		exemplars.visit(visitor);

		verify(visitor).visit(exemplar1);
		verify(visitor).visit(exemplar2);
		verifyNoMoreInteractions(visitor);
	}

	@Test
	public void visitorsTest() {
		ExemplarVisitor visitor1 = mock(ExemplarVisitor.class);
		ExemplarVisitor visitor2 = mock(ExemplarVisitor.class);
		Exemplar exemplar = mock(Exemplar.class);

		ExemplarVisitors visitors = new ExemplarVisitors();
		visitors.add(visitor1);
		visitors.add(visitor2);
		visitors.visit(exemplar);

		verify(visitor1).visit(exemplar);
		verify(visitor2).visit(exemplar);
		verifyNoMoreInteractions(visitor1, visitor2, exemplar);
	}
}
