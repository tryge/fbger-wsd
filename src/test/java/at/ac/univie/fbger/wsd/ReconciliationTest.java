package at.ac.univie.fbger.wsd;

import at.ac.univie.fbger.wsd.impl.DefaultReconciliationFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertSame;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author michael.zehender@me.com
 */
public class ReconciliationTest {
	private ReconciliationRuleFactory ruleFactory;
	private DefaultReconciliationFactory factory;

	@Before
	public void setUp() {
		ruleFactory = mock(ReconciliationRuleFactory.class);

		factory = new DefaultReconciliationFactory();
		factory.setFactory(ruleFactory);
	}

	@Test
	public void givenReconciliationConfigurationThenInstantiateIt() {
		ReconciliationRule rule = mock(ReconciliationRule.class);

		when(ruleFactory.create("a = b")).thenReturn(rule);

		String config = "OutputFile: testOut.txt\nRules:\n  a = b";
		Reconciliation reconciliation = factory.create(config);

		assertNotNull(reconciliation);
		assertEquals("testOut.txt", reconciliation.getOutputFile());

		List<ReconciliationRule> rules = reconciliation.getRules();

		assertNotNull(rules);
		assertEquals(1, rules.size());
		assertSame(rule, rules.get(0));
	}

	@Test
	public void givenReconciliationWhenToSQLThenItIsGenerated() {
		ReconciliationRule rule1 = mock(ReconciliationRule.class);
		ReconciliationRule rule2 = mock(ReconciliationRule.class);

		when(rule1.toSQL()).thenReturn("1");
		when(rule2.toSQL()).thenReturn("2");

		List<ReconciliationRule> rules = new ArrayList<ReconciliationRule>();
		rules.add(rule1);
		rules.add(rule2);

		Reconciliation reconciliation = new Reconciliation("output", rules);
		assertEquals("1 AND 2", reconciliation.toSQL());
	}
}
