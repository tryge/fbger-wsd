package at.ac.univie.fbger.wsd.workflow;

import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.InputStream;
import java.util.concurrent.ExecutorService;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author michael.zehender@me.com
 */
public class MainWorkflowTest {
	private ExecutorService executor;
	private EventBus bus;

	private InputStream wsdIs;
	private WsdImportWorkflow wsdWorkflow;
	private InputStream endnoteIs;
	private EndnoteImportWorkflow endnoteWorkflow;
	private ProcessWorkflow processWorkflow;

	private MainWorkflow workflow;

	@Before
	public void setUp() {
		executor = mock(ExecutorService.class);
		bus = mock(EventBus.class);
		wsdIs = mock(InputStream.class);
		wsdWorkflow = mock(WsdImportWorkflow.class);
		endnoteIs = mock(InputStream.class);
		endnoteWorkflow = mock(EndnoteImportWorkflow.class);
		processWorkflow = mock(ProcessWorkflow.class);

		workflow = new MainWorkflow(executor, bus);
		workflow.setWsdInputStream(wsdIs);
		workflow.setWsdWorkflow(wsdWorkflow);
		workflow.setEndnoteInputStream(endnoteIs);
		workflow.setEndnoteWorkflow(endnoteWorkflow);
		workflow.addProcessWorkflow(processWorkflow);
	}

	@Test
	public void registerToBus() {
		verify(bus).register(workflow);
	}

	@Test
	public void testMainWorkflow() {
		workflow.execute();

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		Runnable runnable = captor.getValue();
		runnable.run();

		verify(wsdWorkflow).execute(wsdIs);
		verify(endnoteWorkflow).execute(endnoteIs);

	}

	@Test
	public void testContinueWorkflow() {
		workflow.finished("WSD");
		workflow.finished("ENDNOTE");

		verify(processWorkflow).execute();
	}

	@Test
	public void testEndWorkflow() {
		workflow.finished("DONE");

		verify(executor).shutdown();
	}

	@Test
	public void testAbortWorkflow() {
		RuntimeException exception = mock(RuntimeException.class);

		workflow.fatal(exception);

		verify(executor).shutdownNow();
	}
}
