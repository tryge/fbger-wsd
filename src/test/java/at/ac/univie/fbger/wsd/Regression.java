package at.ac.univie.fbger.wsd;

import at.ac.univie.fbger.wsd.imp.*;
import at.ac.univie.fbger.wsd.impl.rules.RulesTest;
import at.ac.univie.fbger.wsd.model.ExemplarsTest;
import at.ac.univie.fbger.wsd.proc.DeleteExemplarProcessorTest;
import at.ac.univie.fbger.wsd.proc.ProcessorTest;
import at.ac.univie.fbger.wsd.workflow.EndnoteImportWorkflowTest;
import at.ac.univie.fbger.wsd.workflow.MainWorkflowTest;
import at.ac.univie.fbger.wsd.workflow.WsdImportWorkflowTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author michael.zehender@me.com
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({
	ReconciliationRuleFactoryTest.class,
	ReconciliationTest.class,
	RulesTest.class,
	ExemplarsTest.class,
	WsdXmlReaderTest.class,
	EndnoteXmlReaderTest.class,
	WsdRepositoryTest.class,
	EndnoteRepositoryTest.class,
	EndnoteImporterTest.class,
	WsdImporterTest.class,
	WsdImportWorkflowTest.class,
	EndnoteImportWorkflowTest.class,
	MainWorkflowTest.class,
	ProcessorTest.class,
	DeleteExemplarProcessorTest.class,
	WsdImportFilterTest.class,
	EndnoteImportFilterTest.class
})
public class Regression {
}
