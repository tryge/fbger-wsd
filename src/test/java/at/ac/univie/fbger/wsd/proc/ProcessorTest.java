package at.ac.univie.fbger.wsd.proc;

import at.ac.univie.fbger.wsd.model.Exemplar;
import at.ac.univie.fbger.wsd.model.ExemplarVisitor;
import com.jolbox.bonecp.BoneCP;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class ProcessorTest {
	@Test
	public void testProcessor() throws SQLException {
		String selectStatement = "select";

		ExemplarVisitor visitor = mock(ExemplarVisitor.class);
		BoneCP connectionPool = mock(BoneCP.class);
		Connection connection = mock(Connection.class);
		PreparedStatement statement = mock(PreparedStatement.class);
		ResultSet resultSet = mock(ResultSet.class);

		when(connectionPool.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(selectStatement)).thenReturn(statement);
		when(statement.executeQuery()).thenReturn(resultSet);
		when(resultSet.next()).thenReturn(true).thenReturn(false);

		when(resultSet.getString("id")).thenReturn("id");
		when(resultSet.getString("author")).thenReturn("author");
		when(resultSet.getString("coauthor")).thenReturn("coauthor");
		when(resultSet.getString("title")).thenReturn("title");
		when(resultSet.getString("subtitle")).thenReturn("subtitle");
		when(resultSet.getString("publisher")).thenReturn("publisher");
		when(resultSet.getString("year_of_publication")).thenReturn("year_of_publication");
		when(resultSet.getString("endnote_author")).thenReturn("endnote_author");
		when(resultSet.getString("endnote_title")).thenReturn("endnote_title");
		when(resultSet.getString("endnote_publisher")).thenReturn("endnote_publisher");
		when(resultSet.getString("endnote_year_of_publication")).thenReturn("endnote_year_of_publication");
		when(resultSet.getString("ac_number")).thenReturn("ac_number");
		when(resultSet.getString("library")).thenReturn("library");
		when(resultSet.getString("current_location")).thenReturn("current_location");
		when(resultSet.getString("signature")).thenReturn("signature");
		when(resultSet.getString("inventory_number")).thenReturn("inventory_number");
		when(resultSet.getString("am_number")).thenReturn("am_number");
		when(resultSet.getString("original_location")).thenReturn("original_location");
		when(resultSet.getString("abstract")).thenReturn("abstract");
		when(resultSet.getString("urls")).thenReturn("urls");

		Processor processor = new Processor(visitor, connectionPool);
		processor.setSelectStatement(selectStatement);
		processor.processReconciliation();

		ArgumentCaptor<Exemplar> captor = ArgumentCaptor.forClass(Exemplar.class);
		verify(visitor).visit(captor.capture());

		Exemplar exemplar = captor.getValue();
		assertEquals("id", exemplar.getId());
		assertEquals("author", exemplar.getAuthor());
		assertEquals("coauthor", exemplar.getCoAuthor());
		assertEquals("title", exemplar.getTitle());
		assertEquals("subtitle", exemplar.getSubtitle());
		assertEquals("publisher", exemplar.getPublisher());
		assertEquals("year_of_publication", exemplar.getYearOfPublication());
		assertEquals("endnote_author", exemplar.getEndnoteAuthor());
		assertEquals("endnote_title", exemplar.getEndnoteTitle());
		assertEquals("endnote_publisher", exemplar.getEndnotePublisher());
		assertEquals("endnote_year_of_publication", exemplar.getEndnoteYearOfPublication());
		assertEquals("ac_number", exemplar.getAcNumber());
		assertEquals("library", exemplar.getLibrary());
		assertEquals("current_location", exemplar.getCurrentLocation());
		assertEquals("signature", exemplar.getSignature());
		assertEquals("inventory_number", exemplar.getInventoryNumber());
		assertEquals("am_number", exemplar.getAmNumber());
		assertEquals("original_location", exemplar.getOriginalLocation());
		assertEquals("abstract", exemplar.getAbstract());
		assertEquals("urls", exemplar.getUrls());

		verify(resultSet, times(2)).next();
		verify(resultSet).close();
		verify(statement).close();
		verify(connection).close();
	}
}
