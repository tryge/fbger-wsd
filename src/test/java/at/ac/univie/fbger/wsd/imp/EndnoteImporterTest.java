package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.sql.SQLException;
import java.util.concurrent.Executor;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteImporterTest {
	private EndnoteExemplar exemplar;
	private Executor executor;
	private EndnoteRepository repository;
	private EventBus bus;

	private EndnoteImporter importer;

	@Before
	public void setUp() {
		exemplar = mock(EndnoteExemplar.class);
		executor = mock(Executor.class);
		repository = mock(EndnoteRepository.class);
		bus = mock(EventBus.class);

		importer = new EndnoteImporter(executor, repository, bus);
	}


	@Test
	public void testImport() throws SQLException {
		Runnable runnable = doVisit();
		runnable.run();

		verify(repository).store(same(exemplar));
	}

	@Test
	public void testImportChecksFilter() throws SQLException {
		EndnoteImportFilter filter = mock(EndnoteImportFilter.class);

		when(filter.applies(same(exemplar))).thenReturn(true);

		importer.addFilter(filter);
		Runnable runnable = doVisit();
		runnable.run();

		verify(repository).store(same(exemplar));
	}

	@Test
	public void testImportChecksFilter_2() throws SQLException {
		EndnoteImportFilter filter = mock(EndnoteImportFilter.class);

		when(filter.applies(same(exemplar))).thenReturn(false);

		importer.addFilter(filter);
		Runnable runnable = doVisit();
		runnable.run();

		verifyZeroInteractions(repository);
	}

	@Test
	public void testImportFailure() throws SQLException {
		Runnable runnable = doVisit();

		SQLException exception = mock(SQLException.class);
		doThrow(exception).when(repository).store(exemplar);

		runnable.run();

		verify(bus).post(same(exception));
	}

	private Runnable doVisit() {
		importer.visit(exemplar);

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		return captor.getValue();
	}
}
