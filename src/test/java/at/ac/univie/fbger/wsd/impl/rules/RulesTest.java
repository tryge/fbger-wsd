package at.ac.univie.fbger.wsd.impl.rules;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * @author michael.zehender@me.com
 */
public class RulesTest {
	@Test
	public void givenExactMatchCreateValidSQLSnippet() {
		ExactMatch exactMatch = new ExactMatch("L.a", "R.b");

		assertEquals("L.a = R.b", exactMatch.toSQL());
	}

	@Test
	public void givenBeginsWithCreateValidSQLSnippet() {
		BeginsWith match = new BeginsWith("L.a", "R.b");

		assertEquals("L.a LIKE CONCAT(R.b, '%')", match.toSQL());
	}
}
