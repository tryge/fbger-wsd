package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.sql.SQLException;
import java.util.concurrent.Executor;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class WsdImporterTest {
	private WsdExemplar exemplar;
	private Executor executor;
	private WsdRepository repository;
	private EventBus bus;

	private WsdImporter importer;

	@Before
	public void setUp() {
		exemplar = mock(WsdExemplar.class);
		executor = mock(Executor.class);
		repository = mock(WsdRepository.class);
		bus = mock(EventBus.class);

		when(exemplar.getLibrary()).thenReturn("FB Germanistik");
		when(exemplar.getSignature()).thenReturn("WSD");

		importer = new WsdImporter(executor, repository, bus);
		importer.addFilter(new WsdLibraryFilter("FB Germanistik"));
		importer.addFilter(new WsdSignatureFilter("WSD"));
	}

	@Test
	public void testImport() throws SQLException {
		Runnable runnable = doVisit();
		runnable.run();

		verify(repository).store(same(exemplar));
	}

	@Test
	public void testImportFailure() throws SQLException {
		Runnable runnable = doVisit();

		SQLException exception = mock(SQLException.class);
		doThrow(exception).when(repository).store(exemplar);

		runnable.run();

		verify(bus).post(same(exception));
	}

	private Runnable doVisit() {
		importer.visit(exemplar);

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		return captor.getValue();
	}
}
