package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author michael.zehender@me.com
 */
public class WsdImportFilterTest {
	private static final String LIBRARY = "FB Germanistik";

	private WsdExemplar exemplar;

	@Before
	public void setUp() {
		exemplar = mock(WsdExemplar.class);
	}

	@Test
	public void testLibraryFilterApplies() {
		when(exemplar.getLibrary()).thenReturn(LIBRARY);

		WsdLibraryFilter filter = new WsdLibraryFilter(LIBRARY);
		boolean applies = filter.applies(exemplar);

		assertTrue(applies);
	}

	@Test
	public void testLibraryFilterDoesNotApply() {
		when(exemplar.getLibrary()).thenReturn("Something else");

		WsdLibraryFilter filter = new WsdLibraryFilter(LIBRARY);
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testLibraryFilterDoesNotApply_Null() {
		when(exemplar.getLibrary()).thenReturn(null);

		WsdLibraryFilter filter = new WsdLibraryFilter(LIBRARY);
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testSignatureFilterApplies() {
		when(exemplar.getSignature()).thenReturn("WSD1");

		WsdSignatureFilter filter = new WsdSignatureFilter("WSD");
		boolean applies = filter.applies(exemplar);

		assertTrue(applies);
	}

	@Test
	public void testSignatureFilterDoesNotApply() {
		when(exemplar.getSignature()).thenReturn("FB ..");

		WsdSignatureFilter filter = new WsdSignatureFilter("WSD");
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testSignatureFilterDoesNotApply_Null() {
		when(exemplar.getSignature()).thenReturn(null);

		WsdSignatureFilter filter = new WsdSignatureFilter("WSD");
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testWsdBlacklistFilterApplies() {
		when(exemplar.getSignature()).thenReturn("WSD 1234");

		WsdBlacklistFilter filter = new WsdBlacklistFilter();
		boolean applies = filter.applies(exemplar);

		assertTrue(applies);
	}

	@Test
	public void testWsdBlacklistFilterDoesNotApply() {
		when(exemplar.getSignature()).thenReturn("WSD 1234");

		WsdBlacklistFilter filter = new WsdBlacklistFilter();
		filter.addToBlacklist("WSD 1234");
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testWsdBlacklistFilterDoesNotApply_2() {
		when(exemplar.getSignature()).thenReturn("WSD 1234");

		WsdBlacklistFilter filter = new WsdBlacklistFilter();
		List<String> blacklist = new ArrayList<String>(1);
		blacklist.add("WSD 1234");
		filter.addToBlacklist(blacklist);
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testWsdBlacklistFilterDoesApply_Null() {
		when(exemplar.getSignature()).thenReturn(null);

		WsdBlacklistFilter filter = new WsdBlacklistFilter();
		boolean applies = filter.applies(exemplar);

		assertTrue(applies);
	}
}
