package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import at.ac.univie.fbger.wsd.model.EndnoteExemplarVisitor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteXmlReaderTest {
	private EndnoteExemplarVisitor visitor;
	private EndnoteExtractor extractor;

	@Before
	public void setUp() {
		visitor = mock(EndnoteExemplarVisitor.class);
		extractor = new EndnoteExtractor(visitor);
	}

	@Test
	public void givenSingleRecordXmlExtractEndnoteExemplar() {
		extractor.read(EndnoteXmlReaderTest.class.getResourceAsStream("/endnote/single-record.xml"));

		ArgumentCaptor<EndnoteExemplar> captor = ArgumentCaptor.forClass(EndnoteExemplar.class);
		verify(visitor).visit(captor.capture());

		EndnoteExemplar exemplar = captor.getValue();
		assertEquals("6588", exemplar.getId());
		assertEquals(1, exemplar.getAuthors().size());
		assertEquals("Alarcon, Pedro Antonio <<de>>", exemplar.getAuthors().get(0));
		assertEquals("Manuel Venegas", exemplar.getTitle());
		assertEquals("WZ 4/5", exemplar.getOriginalLocation());
		assertEquals("Spemann", exemplar.getPublisher());
		assertNull(exemplar.getAbstract());
		assertEquals("1748", exemplar.getYearOfPublication());
	}

	@Test
	public void givenTwoRecordsExtractTwoEndnoteExemplars() {
		extractor.read(EndnoteXmlReaderTest.class.getResourceAsStream("/endnote/two-records.xml"));

		ArgumentCaptor<EndnoteExemplar> captor = ArgumentCaptor.forClass(EndnoteExemplar.class);
		verify(visitor, times(2)).visit(captor.capture());

		EndnoteExemplar exemplar = captor.getAllValues().get(0);
		assertEquals("6588", exemplar.getId());
		assertEquals(1, exemplar.getAuthors().size());
		assertEquals("Alarcon, Pedro Antonio <<de>>", exemplar.getAuthors().get(0));
		assertEquals("Manuel Venegas", exemplar.getTitle());
		assertEquals("WZ 4/5", exemplar.getOriginalLocation());
		assertEquals("Spemann", exemplar.getPublisher());
		assertNull(exemplar.getAbstract());
		assertEquals("1748", exemplar.getYearOfPublication());

		EndnoteExemplar exemplar2 = captor.getAllValues().get(1);
		assertEquals("13527", exemplar2.getId());
		assertEquals(1, exemplar2.getAuthors().size());
		assertEquals("Anzengruber, Ludwig", exemplar2.getAuthors().get(0));
		assertEquals("Ludwig Anzengrubers saemtliche Werke", exemplar2.getTitle());
		assertEquals("kritisch durchgesehene Gesamtausgabe in 15 Baenden", exemplar2.getSubtitle());
		assertEquals("VZ 2/1", exemplar2.getOriginalLocation());
		assertEquals("eingelegt Band 3: Fahrscheine; eingelegt Band 5: Ausschnitt: Volkstheater \"Das vierte Gebot\" Inszenierung v. Gustav Manker; jeder Band \"ex libris Baronis Maximiliani de Mayr\"; eingelegt Band 15/3: Rechnung", exemplar2.getAbstract());
		assertEquals("Schroll", exemplar2.getPublisher());
		assertEquals("1749", exemplar2.getYearOfPublication());
	}

	@Test
	public void testExtensiveWhitespaceExemplar() {
		extractor.read(EndnoteXmlReaderTest.class.getResourceAsStream("/endnote/whitespace.xml"));

		ArgumentCaptor<EndnoteExemplar> captor = ArgumentCaptor.forClass(EndnoteExemplar.class);
		verify(visitor).visit(captor.capture());

		EndnoteExemplar exemplar = captor.getValue();
		assertEquals("this is an example for extensive [\\] whitespace", exemplar.getSubtitle());
	}

	@Test
	public void testImportingOfUrls() {
		extractor.read(EndnoteXmlReaderTest.class.getResourceAsStream("/endnote/urls.xml"));

		ArgumentCaptor<EndnoteExemplar> captor = ArgumentCaptor.forClass(EndnoteExemplar.class);
		verify(visitor).visit(captor.capture());

		EndnoteExemplar exemplar = captor.getValue();
		List<String> urls = exemplar.getUrls();
		assertEquals(2, urls.size());
		assertEquals("internal-pdf://notiz_artmann_rosen_Bd1-2986992897/notiz_artmann_rosen_Bd1.jpg", urls.get(0));
		assertEquals("internal-pdf://notiz_artmann_rosen_Bd3-0889845249/notiz_artmann_rosen_Bd3.jpg", urls.get(1));
	}
}
