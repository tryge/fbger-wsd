package at.ac.univie.fbger.wsd;

import at.ac.univie.fbger.wsd.impl.rules.BeginsWith;
import at.ac.univie.fbger.wsd.impl.DefaultReconciliationRuleFactory;
import at.ac.univie.fbger.wsd.impl.rules.ExactMatch;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * @author michael.zehender@me.com
 */
public class ReconciliationRuleFactoryTest {
	private ReconciliationRuleFactory factory;

	@Before
	public void setUp() {
		factory = new DefaultReconciliationRuleFactory();
	}

	@Test(expected = IllegalArgumentException.class)
	public void givenEmptyStringThenThrow() {
		factory.create("");
	}

	@Test
	public void givenSingleFieldMatchInstantiateIt() {
		String ruleSpec = "L.a = R.b";

		ReconciliationRule rule = factory.create(ruleSpec);

		assertNotNull(rule);
		assertTrue(rule instanceof ExactMatch);

		ExactMatch exactMatch = (ExactMatch)rule;
		assertEquals("L.a", exactMatch.getLeftHandSide());
		assertEquals("R.b", exactMatch.getRightHandSide());
	}

	@Test
	public void givenBeginsWithMatchInstantiateIt() {
		String ruleSpec = "L.a beginsWith R.b";

		ReconciliationRule rule = factory.create(ruleSpec);

		assertNotNull(rule);
		assertTrue(rule instanceof BeginsWith);

		BeginsWith beginsWith= (BeginsWith)rule;
		assertEquals("L.a", beginsWith.getLeftHandSide());
		assertEquals("R.b", beginsWith.getRightHandSide());
	}
}
