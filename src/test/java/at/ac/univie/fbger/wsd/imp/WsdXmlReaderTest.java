package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import at.ac.univie.fbger.wsd.model.WsdExemplarVisitor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class WsdXmlReaderTest {
	private WsdExemplarVisitor visitor;
	private WsdExtractor extractor;

	@Before
	public void setUp() {
		visitor = mock(WsdExemplarVisitor.class);
		extractor = new WsdExtractor(visitor);
	}

	@Test
	public void givenSingleRecordXmlExtractWsdExemplar() {
		extractor.read(WsdXmlReaderTest.class.getResourceAsStream("/wsd/single-record.xml"));

		ArgumentCaptor<WsdExemplar> captor = ArgumentCaptor.forClass(WsdExemplar.class);
		verify(visitor).visit(captor.capture());

		WsdExemplar exemplar = captor.getValue();
		assertEquals("Goethe, Johann Wolfgang <<von>>", exemplar.getAuthor());
		assertEquals("Happy Birthday, Goethe!", exemplar.getTitle());
		assertEquals("ein Amadeus-Lesebuch zum 250. Geburtstag", exemplar.getSubtitle());
		assertEquals("AC08959203", exemplar.getAcNumber());
		assertEquals("FB Germanistik", exemplar.getLibrary());
		assertEquals("WSD2", exemplar.getLocatedAt());
		assertEquals("WSD A/1999,10", exemplar.getSignature());
		assertEquals("2012/145-3093", exemplar.getInventoryNumber());
		assertEquals("+AM468087900", exemplar.getAmNumber());
		assertEquals("Echo", exemplar.getPublisher());
		assertEquals("1999", exemplar.getYearOfPublication());
		assertEquals("Bollack, Jean", exemplar.getCoAuthor());
	}

	@Test
	public void givenSingleRecordWithTwoExemplarsThenExtractBothWsdExemplars() {
		extractor.read(WsdXmlReaderTest.class.getResourceAsStream("/wsd/double-exemplar-record.xml"));

		ArgumentCaptor<WsdExemplar> captor = ArgumentCaptor.forClass(WsdExemplar.class);
		verify(visitor, times(2)).visit(captor.capture());

		String author = "Doderer, Heimito <<von>>";
		String title = "<<Das>> letzte Abenteuer";
		String subtitle = "ein Ritter-Roman ; mit einem autobiographischen Nachwort";
		String acNumber = "AC01645576";

		WsdExemplar exemplar1 = captor.getAllValues().get(0);
		assertEquals(author, exemplar1.getAuthor());
		assertEquals(title, exemplar1.getTitle());
		assertEquals(subtitle, exemplar1.getSubtitle());
		assertEquals(acNumber, exemplar1.getAcNumber());
		assertEquals("FB Germanistik", exemplar1.getLibrary());
		assertEquals("WSD2", exemplar1.getLocatedAt());
		assertEquals("WSD A/1981,3", exemplar1.getSignature());
		assertEquals("2012/145-1295", exemplar1.getInventoryNumber());
		assertEquals("+AM46548320X", exemplar1.getAmNumber());
		assertEquals("Reclam", exemplar1.getPublisher());
		assertEquals("1981", exemplar1.getYearOfPublication());
		assertNull(exemplar1.getCoAuthor());

		WsdExemplar exemplar2 = captor.getAllValues().get(1);
		assertEquals(author, exemplar2.getAuthor());
		assertEquals(title, exemplar2.getTitle());
		assertEquals(subtitle, exemplar2.getSubtitle());
		assertEquals(acNumber, exemplar2.getAcNumber());
		assertEquals("Hauptbibliothek", exemplar2.getLibrary());
		assertEquals("NT7", exemplar2.getLocatedAt());
		assertEquals("", exemplar2.getSignature());
		assertEquals("", exemplar2.getInventoryNumber());
		assertEquals("+AM22457209", exemplar2.getAmNumber());
		assertEquals("Reclam", exemplar2.getPublisher());
		assertEquals("1981", exemplar2.getYearOfPublication());
		assertNull(exemplar2.getCoAuthor());
	}

	@Test
	public void givenSeriesRecordThenExtractIt() {
		extractor.read(WsdXmlReaderTest.class.getResourceAsStream("/wsd/series-record.xml"));

		ArgumentCaptor<WsdExemplar> captor = ArgumentCaptor.forClass(WsdExemplar.class);
		verify(visitor).visit(captor.capture());

		WsdExemplar exemplar = captor.getValue();
		assertEquals("Grimminger, Rolf", exemplar.getAuthor());
		assertEquals("Hansers Sozialgeschichte der deutschen Literatur vom 16. Jahrhundert bis zur Gegenwart", exemplar.getSeriesTitle());
		assertEquals("Die Literatur der DDR", exemplar.getTitle());
		assertNull(exemplar.getSubtitle());
		assertEquals("AC00432156", exemplar.getAcNumber());
		assertEquals("AC00431461", exemplar.getSeriesAcNumber());
		assertEquals("FB Germanistik", exemplar.getLibrary());
		assertEquals("WSD1", exemplar.getLocatedAt());
		assertEquals("WSD", exemplar.getSignature());
		assertEquals("2012/145-3861", exemplar.getInventoryNumber());
		assertEquals("+AM467525804", exemplar.getAmNumber());
		assertEquals("Dt. Taschenbuch-Verl.", exemplar.getPublisher());
		assertEquals("1983", exemplar.getYearOfPublication());
		assertNull(exemplar.getCoAuthor());
	}
}
