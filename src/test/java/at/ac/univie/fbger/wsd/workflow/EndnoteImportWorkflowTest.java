package at.ac.univie.fbger.wsd.workflow;

import at.ac.univie.fbger.wsd.imp.EndnoteExtractor;
import at.ac.univie.fbger.wsd.imp.EndnoteImporter;
import com.google.common.eventbus.EventBus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.InputStream;
import java.util.concurrent.Executor;

import static org.mockito.Matchers.same;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteImportWorkflowTest {
	private Executor executor;
	private EventBus bus;
	private EndnoteExtractor extractor;
	private InputStream is;
	private EndnoteImporter importer;

	private EndnoteImportWorkflow workflow;

	@Before
	public void setUp() {
		executor = mock(Executor.class);
		bus = mock(EventBus.class);
		extractor = mock(EndnoteExtractor.class);
		is = mock(InputStream.class);
		importer = mock(EndnoteImporter.class);

		workflow = new EndnoteImportWorkflow(executor, extractor, importer, bus);
	}

	@Test
	public void testEndnoteImport() throws InterruptedException {
		workflow.execute(is);

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		Runnable runnable = captor.getValue();
		runnable.run();

		verify(extractor).read(same(is));
		verify(importer).awaitFinished();
		verify(bus).post("ENDNOTE");
	}

	@Test
	public void testEndnoteImportFailure() {
		workflow.execute(is);

		ArgumentCaptor<Runnable> captor = ArgumentCaptor.forClass(Runnable.class);
		verify(executor).execute(captor.capture());

		Runnable runnable = captor.getValue();

		RuntimeException exception = mock(RuntimeException.class);
		doThrow(exception).when(extractor).read(same(is));

		runnable.run();

		verify(bus).post(exception);
	}
}
