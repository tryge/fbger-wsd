package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import com.jolbox.bonecp.BoneCP;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class WsdRepositoryTest {
	private static final String INSERT_SQL = "INSERT INTO wsd";

	@Test
	public void givenWsdExemplarThenStoreIt() throws SQLException {
		WsdExemplar exemplar = new WsdExemplar();
		exemplar.setSeriesAcNumber("seriesAc");
		exemplar.setAcNumber("ac");
		exemplar.setAmNumber("am");
		exemplar.setAuthor("author");
		exemplar.setCoAuthor("coauthor");
		exemplar.setSubtitle("subtitle");
		exemplar.setSeriesTitle("seriesTitle");
		exemplar.setTitle("title, a");
		exemplar.setPublisher("publisher");
		exemplar.setInventoryNumber("inventory");
		exemplar.setLibrary("library");
		exemplar.setLocatedAt("locatedAt");
		exemplar.setSignature("signature");
		exemplar.setYearOfPublication("1000");

		BoneCP dataSource = mock(BoneCP.class);
		Connection connection = mock(Connection.class);
		PreparedStatement statement = mock(PreparedStatement.class);

		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(INSERT_SQL)).thenReturn(statement);
		when(statement.executeUpdate()).thenReturn(1);

		WsdRepository repository = new WsdRepository(dataSource);
		repository.setInsertSQL(INSERT_SQL);
		repository.store(exemplar);

		verify(statement).setString(1, "WSD-1");
		verify(statement).setString(2, "ac");
		verify(statement).setString(3, "am");
		verify(statement).setString(4, "author");
		verify(statement).setString(5, "coauthor");
		verify(statement).setString(6, "inventory");
		verify(statement).setString(7, "library");
		verify(statement).setString(8, "locatedAt");
		verify(statement).setString(9, "publisher");
		verify(statement).setString(10, "seriesAc");
		verify(statement).setString(11, "seriesTitle");
		verify(statement).setString(12, "seriesTitle");
		verify(statement).setString(13, "signature");
		verify(statement).setString(14, "subtitle");
		verify(statement).setString(15, "title, a");
		verify(statement).setString(16, "title a");
		verify(statement).setString(17, "1000");
		verify(statement).executeUpdate();
		verify(statement).close();
		verify(connection).close();
	}

	@Test
	public void givenEmptyWsdExemplarThenStoreIt() throws SQLException {
		WsdExemplar exemplar = new WsdExemplar();

		BoneCP dataSource = mock(BoneCP.class);
		Connection connection = mock(Connection.class);
		PreparedStatement statement = mock(PreparedStatement.class);

		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(INSERT_SQL)).thenReturn(statement);
		when(statement.executeUpdate()).thenReturn(1);

		WsdRepository repository = new WsdRepository(dataSource);
		repository.setInsertSQL(INSERT_SQL);
		repository.store(exemplar);

		verify(statement).setString(1, "WSD-1");
		verify(statement).setNull(2, Types.VARCHAR);
		verify(statement).setNull(3, Types.VARCHAR);
		verify(statement).setNull(4, Types.VARCHAR);
		verify(statement).setNull(5, Types.VARCHAR);
		verify(statement).setNull(6, Types.VARCHAR);
		verify(statement).setNull(7, Types.VARCHAR);
		verify(statement).setNull(8, Types.VARCHAR);
		verify(statement).setNull(9, Types.VARCHAR);
		verify(statement).setNull(10, Types.VARCHAR);
		verify(statement).setNull(11, Types.VARCHAR);
		verify(statement).setNull(12, Types.VARCHAR);
		verify(statement).setNull(13, Types.VARCHAR);
		verify(statement).setNull(14, Types.VARCHAR);
		verify(statement).setNull(15, Types.VARCHAR);
		verify(statement).setNull(16, Types.VARCHAR);
		verify(statement).executeUpdate();
		verify(statement).close();
		verify(connection).close();
	}
}
