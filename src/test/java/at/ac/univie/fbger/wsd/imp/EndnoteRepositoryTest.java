package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import com.jolbox.bonecp.BoneCP;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;

import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteRepositoryTest {
	private static final String INSERT_SQL = "INSERT INTO endnote";

	@Test
	public void givenEndnoteExemplarThenStoreIt() throws SQLException {
		EndnoteExemplar exemplar = new EndnoteExemplar();
		exemplar.setId("6588");
		exemplar.setAbstract("abstract");
		exemplar.addAuthor("author");
		exemplar.setOriginalLocation("originalLocation");
		exemplar.setTitle("title, a");
		exemplar.setSubtitle("subtitle");
		exemplar.setPublisher("publisher");
		exemplar.setYearOfPublication("1999");
		exemplar.addUrl("url");

		BoneCP dataSource = mock(BoneCP.class);
		Connection connection = mock(Connection.class);
		PreparedStatement statement = mock(PreparedStatement.class);

		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(INSERT_SQL)).thenReturn(statement);

		EndnoteRepository repository = new EndnoteRepository(dataSource);
		repository.setInsertSQL(INSERT_SQL);
		repository.store(exemplar);

		verify(connection).prepareStatement(INSERT_SQL);
		verify(statement).setString(1, "abstract");
		verify(statement).setString(2, "author");
		verify(statement).setString(3, "originalLocation");
		verify(statement).setString(4, "publisher");
		verify(statement).setString(5, "subtitle");
		verify(statement).setString(6, "title, a");
		verify(statement).setString(7, "title a");
		verify(statement).setString(8, "url");
		verify(statement).setString(9, "1999");
		verify(statement).setString(10, "6588");
		verify(statement).executeUpdate();
		verify(statement).close();
		verify(connection).close();
		verifyNoMoreInteractions(statement, connection);
	}

	@Test
	public void givenEmptyEndnoteExemplarStoreIt() throws SQLException {
		EndnoteExemplar exemplar = new EndnoteExemplar();

		BoneCP dataSource = mock(BoneCP.class);
		Connection connection = mock(Connection.class);
		PreparedStatement statement = mock(PreparedStatement.class);

		when(dataSource.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(INSERT_SQL)).thenReturn(statement);

		EndnoteRepository repository = new EndnoteRepository(dataSource);
		repository.setInsertSQL(INSERT_SQL);
		repository.store(exemplar);

		verify(connection).prepareStatement(INSERT_SQL);
		verify(statement).setNull(1, Types.VARCHAR);
		verify(statement).setString(2, "");
		verify(statement).setNull(3, Types.VARCHAR);
		verify(statement).setNull(4, Types.VARCHAR);
		verify(statement).setNull(5, Types.VARCHAR);
		verify(statement).setNull(6, Types.VARCHAR);
		verify(statement).setNull(7, Types.VARCHAR);
		verify(statement).setString(8, "");
		verify(statement).setNull(9, Types.VARCHAR);
		verify(statement).setNull(10, Types.VARCHAR);
		verify(statement).executeUpdate();
		verify(statement).close();
		verify(connection).close();
		verifyNoMoreInteractions(statement, connection);
	}
}
