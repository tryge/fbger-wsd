package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteImportFilterTest {
	private EndnoteExemplar exemplar;

	@Before
	public void setUp() {
		exemplar = mock(EndnoteExemplar.class);
	}

	@Test
	public void testEndnoteBlacklistFilterApplies() {
		when(exemplar.getId()).thenReturn("1234");

		EndnoteBlacklistFilter filter = new EndnoteBlacklistFilter();
		boolean applies = filter.applies(exemplar);

		assertTrue(applies);
	}

	@Test
	public void testEndnoteBlacklistFilterDoesNotApply() {
		when(exemplar.getId()).thenReturn("WSD 1234");

		EndnoteBlacklistFilter filter = new EndnoteBlacklistFilter();
		filter.addToBlacklist("WSD 1234");
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testEndnoteBlacklistFilterDoesNotApply_2() {
		when(exemplar.getId()).thenReturn("WSD 1234");

		EndnoteBlacklistFilter filter = new EndnoteBlacklistFilter();
		List<String> blacklist = new ArrayList<String>(1);
		blacklist.add("WSD 1234");
		filter.addToBlacklist(blacklist);
		boolean applies = filter.applies(exemplar);

		assertFalse(applies);
	}

	@Test
	public void testEndnoteBlacklistFilterDoesApply_Null() {
		when(exemplar.getId()).thenReturn(null);

		EndnoteBlacklistFilter filter = new EndnoteBlacklistFilter();
		boolean applies = filter.applies(exemplar);

		assertTrue(applies);
	}
}
