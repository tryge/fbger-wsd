package at.ac.univie.fbger.wsd.proc;

import at.ac.univie.fbger.wsd.model.Exemplar;
import com.google.common.eventbus.EventBus;
import com.jolbox.bonecp.BoneCP;
import org.junit.Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static org.mockito.Mockito.*;

/**
 * @author michael.zehender@me.com
 */
public class DeleteExemplarProcessorTest {
	private final static String STATEMENT = "DELETE FROM";
	private final static String ID = "ID";

	@Test
	public void testDeleteExemplar() throws SQLException {
		EventBus bus = mock(EventBus.class);
		BoneCP connectionPool = mock(BoneCP.class);
		Connection connection = mock(Connection.class);
		PreparedStatement statement = mock(PreparedStatement.class);
		Exemplar exemplar = mock(Exemplar.class);

		when(connectionPool.getConnection()).thenReturn(connection);
		when(connection.prepareStatement(STATEMENT)).thenReturn(statement);
		when(exemplar.getId()).thenReturn(ID);

		DeleteExemplarProcessor processor = new DeleteExemplarProcessor(connectionPool, bus);
		processor.setDeleteSql(STATEMENT);
		processor.visit(exemplar);

		verify(connectionPool).getConnection();
		verify(connection).prepareStatement(STATEMENT);
		verify(statement).setString(1, ID);
		verify(statement).executeUpdate();
		verify(statement).close();
		verify(connection).close();
	}
}
