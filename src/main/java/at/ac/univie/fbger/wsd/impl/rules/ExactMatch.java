package at.ac.univie.fbger.wsd.impl.rules;

import at.ac.univie.fbger.wsd.ReconciliationRule;

/**
 * @author michael.zehender@me.com
 */
public class ExactMatch implements ReconciliationRule {
	private final String lhs;
	private final String rhs;

	public ExactMatch(String lhs, String rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public String getLeftHandSide() {
		return lhs;
	}

	public String getRightHandSide() {
		return rhs;
	}

	@Override
	public String toSQL() {
		StringBuilder builder = new StringBuilder();
		builder.append(lhs)
			.append(" = ")
			.append(rhs);

		return builder.toString();
	}
}
