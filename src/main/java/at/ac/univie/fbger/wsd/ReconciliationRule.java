package at.ac.univie.fbger.wsd;

/**
 * @author michael.zehender@me.com
 */
public interface ReconciliationRule {
	/**
	 * Generates a valid SQL snippet from this rule (i.e. the rule is
	 * expressed as SQL clauses.
	 *
	 * @return the generated SQL snippet
	 */
	String toSQL();
}
