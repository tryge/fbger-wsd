package at.ac.univie.fbger.wsd;

/**
 * @author michael.zehender@me.com
 */
public interface ReconciliationFactory {
	Reconciliation create(String config);
}
