package at.ac.univie.fbger.wsd.impl;

import at.ac.univie.fbger.wsd.Reconciliation;
import at.ac.univie.fbger.wsd.ReconciliationFactory;
import at.ac.univie.fbger.wsd.ReconciliationRule;
import at.ac.univie.fbger.wsd.ReconciliationRuleFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @author michael.zehender@me.com
 */
public class DefaultReconciliationFactory implements ReconciliationFactory {
	@Override
	public Reconciliation create(String config) {
		String[] lines = config.split("\n");

		String outputFile = null;
		List<ReconciliationRule> rules = new ArrayList<ReconciliationRule>();

		for (String line : lines) {
			line = line.trim();
			if (line.startsWith("OutputFile:")) {
				outputFile = line.substring(line.indexOf(':') + 1).trim();
			} else if (line.startsWith("Rules:")) {
				// do nothing, is just there for readability
			} else {
				rules.add(factory.create(line));
			}
		}

		return new Reconciliation(outputFile, rules);
	}

	// INJECTED
	private ReconciliationRuleFactory factory;

	public void setFactory(ReconciliationRuleFactory factory) {
		this.factory = factory;
	}
}
