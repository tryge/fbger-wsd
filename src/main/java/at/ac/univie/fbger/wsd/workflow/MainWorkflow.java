package at.ac.univie.fbger.wsd.workflow;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.io.InputStream;
import java.util.List;
import java.util.concurrent.ExecutorService;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author michael.zehender@me.com
 */
public class MainWorkflow {
	private final ExecutorService executor;
	private final EventBus bus;

	private volatile boolean wsdFinished;
	private volatile boolean endnoteFinished;

	private volatile int workflowsDone = 0;

	public MainWorkflow(ExecutorService executor, EventBus bus) {
		this.executor = executor;
		this.bus = bus;
		this.bus.register(this);
	}

	public void execute() {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				endnoteWorkflow.execute(endnoteInputStream);
				wsdWorkflow.execute(wsdInputStream);
			}
		});
	}

	@Subscribe
	public void finished(String what) {
		if ("SHUTDOWN".equals(what)) {
			return;
		} else if ("WSD".equals(what)) {
			wsdFinished = true;
		} else if("ENDNOTE".equals(what)) {
			endnoteFinished = true;
		} else if("DONE".equals(what)) {
			if (++workflowsDone == processWorkflows.size()) {
				try {
					if (shutdownWorkflow != null) {
						shutdownWorkflow.execute();
					}
				} catch(Exception e) {
					e.printStackTrace();
				}
				bus.post("SHUTDOWN");
				executor.shutdown();
				return;
			}
		}

		if (wsdFinished && endnoteFinished) {
			try {
				nextWorkflow().execute();
			} catch (RuntimeException e) {
				bus.post(e);
			}
		}
	}

	private ProcessWorkflow nextWorkflow() {
		return processWorkflows.get(workflowsDone);
	}

	@Subscribe
	public void fatal(Exception exception) {
		System.err.println("Cannot perform reconciliation:");
		exception.printStackTrace();

		executor.shutdownNow();
	}

	// INJECTED
	private InputStream wsdInputStream;
	private WsdImportWorkflow wsdWorkflow;
	private InputStream endnoteInputStream;
	private EndnoteImportWorkflow endnoteWorkflow;
	private Workflow shutdownWorkflow;
	private List<ProcessWorkflow> processWorkflows = newArrayList();

	public void setWsdInputStream(InputStream wsdInputStream) {
		this.wsdInputStream = wsdInputStream;
	}

	public void setWsdWorkflow(WsdImportWorkflow wsdWorkflow) {
		this.wsdWorkflow = wsdWorkflow;
	}

	public void setEndnoteInputStream(InputStream endnoteInputStream) {
		this.endnoteInputStream = endnoteInputStream;
	}

	public void setEndnoteWorkflow(EndnoteImportWorkflow endnoteWorkflow) {
		this.endnoteWorkflow = endnoteWorkflow;
	}

	public void setShutdownWorkflow(Workflow shutdownWorkflow) {
		this.shutdownWorkflow = shutdownWorkflow;
	}

	public void addProcessWorkflow(ProcessWorkflow processWorkflow) {
		processWorkflows.add(processWorkflow);
	}
}
