package at.ac.univie.fbger.wsd.model;

/**
 * This is a single {@code Exemplar} included in a rule's output, providing
 * the necessary data to further process it.
 *
 * @author michael.zehender@me.com
 */
public class Exemplar {
	//<editor-fold desc="id">
	private String id;
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	//</editor-fold>
	//<editor-fold desc="author">
	private String author;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	//</editor-fold>
	//<editor-fold desc="coAuthor">
	private String coAuthor;

	public String getCoAuthor() {
		return coAuthor;
	}

	public void setCoAuthor(String coAuthor) {
		this.coAuthor = coAuthor;
	}
	//</editor-fold>
	//<editor-fold desc="publisher">
	private String publisher;

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	//</editor-fold>
	//<editor-fold desc="yearOfPublication">
	private String yearOfPublication;

	public String getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(String yearOfPublication) {
		this.yearOfPublication = yearOfPublication;
	}
	//</editor-fold>
	//<editor-fold desc="seriesTitle">
	private String seriesTitle;

	public String getSeriesTitle() {
		return seriesTitle;
	}

	public void setSeriesTitle(String seriesTitle) {
		this.seriesTitle = seriesTitle;
	}
	//</editor-fold>
	//<editor-fold desc="title">
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	//</editor-fold>
	//<editor-fold desc="subtitle">
	private String subtitle;

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	//</editor-fold>
	//<editor-fold desc="endnoteId">
	private String endnoteId;

	public String getEndnoteId() {
		return endnoteId;
	}

	public void setEndnoteId(String endnoteId) {
		this.endnoteId = endnoteId;
	}
	//</editor-fold>
	//<editor-fold desc="endnoteAuthor">
	private String endnoteAuthor;

	public String getEndnoteAuthor() {
		return endnoteAuthor;
	}

	public void setEndnoteAuthor(String endnoteAuthor) {
		this.endnoteAuthor = endnoteAuthor;
	}
	//</editor-fold>
	//<editor-fold desc="endnoteTitle">
	private String endnoteTitle;

	public String getEndnoteTitle() {
		return endnoteTitle;
	}

	public void setEndnoteTitle(String endnoteTitle) {
		this.endnoteTitle = endnoteTitle;
	}
	//</editor-fold>
	//<editor-fold desc="endnotePublisher">
	private String endnotePublisher;

	public String getEndnotePublisher() {
		return endnotePublisher;
	}

	public void setEndnotePublisher(String endnotePublisher) {
		this.endnotePublisher = endnotePublisher;
	}
	//</editor-fold>
	//<editor-fold desc="endnoteYearOfPublication">
	private String endnoteYearOfPublication;

	public String getEndnoteYearOfPublication() {
		return endnoteYearOfPublication;
	}

	public void setEndnoteYearOfPublication(String endnoteYearOfPublication) {
		this.endnoteYearOfPublication = endnoteYearOfPublication;
	}
	//</editor-fold>
	//<editor-fold desc="seriesAcNumber">
	private String seriesAcNumber;

	public String getSeriesAcNumber() {
		return seriesAcNumber;
	}

	public void setSeriesAcNumber(String seriesAcNumber) {
		this.seriesAcNumber = seriesAcNumber;
	}
	//</editor-fold>
	//<editor-fold desc="acNumber">
	private String acNumber;

	public String getAcNumber() {
		return acNumber;
	}

	public void setAcNumber(String acNumber) {
		this.acNumber = acNumber;
	}
	//</editor-fold>
	//<editor-fold desc="library">
	private String library;

	public String getLibrary() {
		return library;
	}

	public void setLibrary(String library) {
		this.library = library;
	}
	//</editor-fold>
	//<editor-fold desc="currentLocation">
	private String currentLocation;

	public String getCurrentLocation() {
		return currentLocation;
	}

	public void setCurrentLocation(String currentLocation) {
		this.currentLocation = currentLocation;
	}
	//</editor-fold>
	//<editor-fold desc="signature">
	private String signature;

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
	//</editor-fold>
	//<editor-fold desc="inventoryNumber">
	private String inventoryNumber;

	public String getInventoryNumber() {
		return inventoryNumber;
	}

	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}
	//</editor-fold>
	//<editor-fold desc="amNumber">
	private String amNumber;

	public String getAmNumber() {
		return amNumber;
	}

	public void setAmNumber(String amNumber) {
		this.amNumber = amNumber;
	}
	//</editor-fold>
	//<editor-fold desc="originalLocation">
	private String originalLocation;

	public String getOriginalLocation() {
		return originalLocation;
	}

	public void setOriginalLocation(String originalLocation) {
		this.originalLocation = originalLocation;
	}
	//</editor-fold>
	//<editor-fold desc="abstractProp">
	private String abstractProp;

	public String getAbstract() {
		return abstractProp;
	}

	public void setAbstract(String abstractProp) {
		this.abstractProp = abstractProp;
	}
	//</editor-fold>
	//<editor-fold desc="urls">
	private String urls;

	public String getUrls() {
		return urls;
	}

	public void setUrls(String urls) {
		this.urls = urls;
	}
	//</editor-fold>
}
