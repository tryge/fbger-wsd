package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.List;

/**
 * @author michael.zehender@me.com
 */
public class WsdBlacklistFilter implements WsdImportFilter {
	private final List<String> blacklist = new ArrayList<String>();

	public void addToBlacklist(String item) {
		blacklist.add(item);
	}

	public void addToBlacklist(List<String> part) {
		blacklist.addAll(part);
	}

	@Override
	public boolean applies(WsdExemplar exemplar) {
		Preconditions.checkNotNull(exemplar);

		return !blacklist.contains(exemplar.getSignature());
	}
}
