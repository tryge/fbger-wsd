package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import com.google.common.base.Preconditions;

/**
 * @author michael.zehender@me.com
 */
public class WsdLibraryFilter implements WsdImportFilter {
	private final String library;

	public WsdLibraryFilter(String library) {
		Preconditions.checkNotNull(library);

		this.library = library;
	}

	@Override
	public boolean applies(WsdExemplar exemplar) {
		return this.library.equals(exemplar.getLibrary());
	}
}
