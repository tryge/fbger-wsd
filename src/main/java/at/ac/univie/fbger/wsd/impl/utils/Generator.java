package at.ac.univie.fbger.wsd.impl.utils;

import com.google.common.collect.Iterables;

import java.util.Iterator;

import static com.google.common.base.Predicates.notNull;

/**
 * @author michael.zehender@me.com
 */
public class Generator<T> implements Iterable<T> {
	private final Iterable<GeneratorFunc<T>> generatorFuncs;

	public Generator(Iterable<GeneratorFunc<T>> generatorFuncs) {
		this.generatorFuncs = Iterables.filter(generatorFuncs, notNull());
	}

	@Override
	public Iterator<T> iterator() {
		return new GeneratorIterator<T>(generatorFuncs.iterator());
	}

	private class GeneratorIterator<T> implements Iterator<T> {
		private final Iterator<GeneratorFunc<T>> iterator;

		private GeneratorIterator(Iterator<GeneratorFunc<T>> iterator) {
			this.iterator = iterator;
		}

		@Override
		public boolean hasNext() {
			return iterator.hasNext();
		}

		@Override
		public T next() {
			return iterator.next().generate();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException("Cannot remove from Generator.");
		}
	}
}
