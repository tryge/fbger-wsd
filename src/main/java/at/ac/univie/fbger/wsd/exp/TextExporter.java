package at.ac.univie.fbger.wsd.exp;

import at.ac.univie.fbger.wsd.model.Exemplar;
import at.ac.univie.fbger.wsd.model.ExemplarVisitor;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author michael.zehender@me.com
 */
public class TextExporter implements ExemplarVisitor {
	private final EventBus bus;
	private final File outputFile;
	private PrintStream out;

	public TextExporter(EventBus bus, File outputFile) {
		this.bus = bus;
		this.bus.register(this);
		this.outputFile = outputFile;
	}

	@Override
	public void visit(Exemplar exemplar) {
		try {
			if (out == null) {
				out = new PrintStream(new FileOutputStream(outputFile));
			}

			out.println("Autor 1: " + value(exemplar.getAuthor()));
			out.println("Autor 2: " + value(exemplar.getCoAuthor()));
			out.println("Gesamttitelwerk: " + value(exemplar.getSeriesTitle()));
			out.println("Titel: " + value(exemplar.getTitle()));
			out.println("Untertitel: " + value(exemplar.getSubtitle()));
			out.println("Verlag: " + value(exemplar.getPublisher()));
			out.println("Erscheinungsjahr: " + value(exemplar.getYearOfPublication()));
			out.println("Gesamt-AC-Nummer:" + value(exemplar.getSeriesAcNumber()));
			out.println("AC-Nummer: " + value(exemplar.getAcNumber()));
			out.println("Bibliothek: " + value(exemplar.getLibrary()));
			out.println("aktueller Standort: " + value(exemplar.getCurrentLocation()));
			out.println("Signatur: " + value(exemplar.getSignature()));
			out.println("Inventarnummer: " + value(exemplar.getInventoryNumber()));
			out.println("AM-Nummer: " + value(exemplar.getAmNumber()));
			out.println("Endnote-Autor: " + value(exemplar.getEndnoteAuthor()));
			out.println("Endnote-Titel: " + value(exemplar.getEndnoteTitle()));
			out.println("Endnote-Verlag: " + value(exemplar.getEndnotePublisher()));
			out.println("Endnote-Erscheinungsjahr: " + value(exemplar.getEndnoteYearOfPublication()));
			out.println("Endnote-RecordNumber: ********** " + value(exemplar.getEndnoteId()));
			out.println("ursprünglicher Standort: " + value(exemplar.getOriginalLocation()));
			out.println("Beilagen:");

			if (exemplar.getAbstract() != null) {
				for (String a : exemplar.getAbstract().split(";")) {
					out.println("\t* " + a.trim());
				}
			} else {
				out.println("\tKeine");
			}

			out.println("Attachements:");

			if (!exemplar.getUrls().equals("")) {
				for (String url : exemplar.getUrls().split(";")) {
					out.println("\t* " + url.trim());
				}
			} else {
				out.println("\tKeine");
			}

			out.println();
		} catch (Exception e) {
			bus.post(e);
		}
	}

	private String value(String value) {
		if (value == null) {
			return "";
		}
		return value;
	}

	@Subscribe
	public void done(String message) {
		if ("SHUTDOWN".equals(message)) {
			if (out != null) {
				out.close();
			}
		}
	}
}
