package at.ac.univie.fbger.wsd.impl.utils;

/**
 * @author michael.zehender@me.com
 */
public interface GeneratorFunc<T> {
	T generate();
}
