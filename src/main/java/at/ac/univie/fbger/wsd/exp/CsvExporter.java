package at.ac.univie.fbger.wsd.exp;

import at.ac.univie.fbger.wsd.model.Exemplar;
import at.ac.univie.fbger.wsd.model.ExemplarVisitor;
import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;

/**
 * @author michael.zehender@me.com
 */
public class CsvExporter implements ExemplarVisitor {
	private final EventBus bus;
	private final File outputFile;
	private final String separator;
	private PrintStream out;

	public CsvExporter(EventBus bus, File outputFile, String separator) {
		this.bus = bus;
		this.bus.register(this);
		this.outputFile = outputFile;
		this.separator = separator;
	}

	@Override
	public void visit(Exemplar exemplar) {
		try {
			if (out == null) {
				out = new PrintStream(new FileOutputStream(outputFile));
				writeHeader();
			}

			StringBuilder entry = new StringBuilder();
			entry.append(value(exemplar.getAuthor())).append(separator)
				.append(value(exemplar.getCoAuthor())).append(separator)
				.append(value(exemplar.getSeriesTitle())).append(separator)
				.append(value(exemplar.getTitle())).append(separator)
				.append(value(exemplar.getSubtitle())).append(separator)
				.append(value(exemplar.getPublisher())).append(separator)
				.append(value(exemplar.getYearOfPublication())).append(separator)
				.append(value(exemplar.getSeriesAcNumber())).append(separator)
				.append(value(exemplar.getAcNumber())).append(separator)
				.append(value(exemplar.getLibrary())).append(separator)
				.append(value(exemplar.getCurrentLocation())).append(separator)
				.append(value(exemplar.getSignature())).append(separator)
				.append(value(exemplar.getInventoryNumber())).append(separator)
				.append(value(exemplar.getAmNumber())).append(separator)
				.append(value(exemplar.getEndnoteAuthor())).append(separator)
				.append(value(exemplar.getEndnoteTitle())).append(separator)
				.append(value(exemplar.getEndnotePublisher())).append(separator)
				.append(value(exemplar.getEndnoteYearOfPublication())).append(separator)
				.append(value(exemplar.getEndnoteId())).append(separator)
				.append(value(exemplar.getOriginalLocation())).append(separator);

			String[] abstracts = value(exemplar.getAbstract()).split(";");
			for (int i = 0; i <= 9; i++) {
				if (i < abstracts.length) {
					entry.append(abstracts[i]);
				}
				entry.append(separator);
			}

			String[] attachements = value(exemplar.getUrls()).split(";");
			for (int i = 0; i <= 9; i++) {
				if (i < attachements.length) {
					entry.append(attachements[i]);
				}
				entry.append(separator);
			}

			out.println(entry.toString());
		} catch (Exception e) {
			bus.post(e);
		}
	}

	private void writeHeader() {
		StringBuilder header = new StringBuilder();
		header.append("Autor 1").append(separator)
			.append("Autor 2").append(separator)
			.append("Gesamttitelwerk").append(separator)
			.append("Titel").append(separator)
			.append("Untertitel").append(separator)
			.append("Verlag").append(separator)
			.append("Erscheinungsjahr").append(separator)
			.append("Gesamt-AC-Nummer").append(separator)
			.append("AC-Nummer").append(separator)
			.append("Bibliothek").append(separator)
			.append("aktueller Standort").append(separator)
			.append("Signatur").append(separator)
			.append("Inventarnummer").append(separator)
			.append("AM-Nummer").append(separator)
			.append("Endnote-Autor").append(separator)
			.append("Endnote-Titel").append(separator)
			.append("Endnote-Verlag").append(separator)
			.append("Endnote-Erscheinungsjahr").append(separator)
			.append("Endnote-RecordNumber").append(separator)
			.append("ursprünglicher Standort").append(separator)
			.append("Beilage 1").append(separator)
			.append("Beilage 2").append(separator)
			.append("Beilage 3").append(separator)
			.append("Beilage 4").append(separator)
			.append("Beilage 5").append(separator)
			.append("Beilage 6").append(separator)
			.append("Beilage 7").append(separator)
			.append("Beilage 8").append(separator)
			.append("Beilage 9").append(separator)
			.append("Beilage 10").append(separator)
			.append("Attachement 1").append(separator)
			.append("Attachement 2").append(separator)
			.append("Attachement 3").append(separator)
			.append("Attachement 4").append(separator)
			.append("Attachement 5").append(separator)
			.append("Attachement 6").append(separator)
			.append("Attachement 7").append(separator)
			.append("Attachement 8").append(separator)
			.append("Attachement 9").append(separator)
			.append("Attachement 10").append(separator);

		out.println(header.toString());
	}

	private String value(String value) {
		if (value == null) {
			return "";
		}
		return value;
	}

	@Subscribe
	public void done(String message) {
		if ("SHUTDOWN".equals(message)) {
			if (out != null) {
				out.close();
			}
		}
	}
}
