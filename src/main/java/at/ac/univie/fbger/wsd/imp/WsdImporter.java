package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import at.ac.univie.fbger.wsd.model.WsdExemplarVisitor;
import com.google.common.eventbus.EventBus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * @author michael.zehender@me.com
 */
public class WsdImporter implements WsdExemplarVisitor {
	private final Executor executor;
	private final WsdRepository repository;
	private final EventBus bus;
	private final List<WsdImportFilter> filters = new ArrayList<WsdImportFilter>();

	private int tasks = 0;

	public WsdImporter(Executor executor, WsdRepository repository, EventBus bus) {
		this.executor = executor;
		this.repository = repository;
		this.bus = bus;
	}

	public void addFilter(WsdImportFilter filter) {
		filters.add(filter);
	}

	@Override
	public synchronized void visit(final WsdExemplar exemplar) {
		tasks++;
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					if (filterApply(exemplar)) {
						repository.store(exemplar);
					}
				} catch (SQLException e) {
					bus.post(e);
				} finally {
					synchronized (WsdImporter.this) {
						tasks--;
						WsdImporter.this.notifyAll();
					}
				}
			}
		});
	}

	private boolean filterApply(WsdExemplar exemplar) {
		for (WsdImportFilter filter : filters) {
			if (!filter.applies(exemplar)) {
				return false;
			}
		}
		return true;
	}

	public synchronized void awaitFinished() throws InterruptedException {
		while (tasks > 0) {
			wait();
		}
	}
}
