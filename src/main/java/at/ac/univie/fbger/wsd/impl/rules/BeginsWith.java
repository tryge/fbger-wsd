package at.ac.univie.fbger.wsd.impl.rules;

import at.ac.univie.fbger.wsd.ReconciliationRule;

/**
 * @author michael.zehender@me.com
 */
public class BeginsWith implements ReconciliationRule {
	private String lhs;
	private String rhs;

	public BeginsWith(String lhs, String rhs) {
		this.lhs = lhs;
		this.rhs = rhs;
	}

	public String getLeftHandSide() {
		return lhs;
	}

	public String getRightHandSide() {
		return rhs;
	}

	@Override
	public String toSQL() {
		StringBuilder builder = new StringBuilder();
		builder.append(lhs)
			.append(" LIKE")
			.append(" CONCAT(")
				.append(rhs)
				.append(", '%'")
			.append(")");

		return builder.toString();
	}
}
