package at.ac.univie.fbger.wsd.model;

import java.util.List;

/**
 * This class is a rule's output i.e. each exemplar matched this rule and is
 * therefore included in {@code Exemplars}.
 *
 * @author michael.zehender@me.com
 */
public class Exemplars {
	private final List<Exemplar> exemplarList;

	public Exemplars(List<Exemplar> exemplarList) {
		this.exemplarList = exemplarList;
	}

	public void visit(ExemplarVisitor visitor) {
		for (Exemplar exemplar : exemplarList) {
			visitor.visit(exemplar);
		}
	}
}
