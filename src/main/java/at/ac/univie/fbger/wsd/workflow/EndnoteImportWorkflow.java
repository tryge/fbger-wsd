package at.ac.univie.fbger.wsd.workflow;

import at.ac.univie.fbger.wsd.imp.EndnoteExtractor;
import at.ac.univie.fbger.wsd.imp.EndnoteImporter;
import com.google.common.eventbus.EventBus;

import java.io.InputStream;
import java.util.concurrent.Executor;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteImportWorkflow {
	private Executor executor;
	private EndnoteExtractor extractor;
	private EndnoteImporter importer;
	private EventBus bus;

	public EndnoteImportWorkflow(Executor executor, EndnoteExtractor extractor, EndnoteImporter importer, EventBus bus) {
		this.executor = executor;
		this.extractor = extractor;
		this.importer = importer;
		this.bus = bus;
	}

	public void execute(final InputStream is) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					extractor.read(is);
					importer.awaitFinished();
					bus.post("ENDNOTE");
				} catch (Exception e) {
					bus.post(e);
				}
			}
		});
	}
}
