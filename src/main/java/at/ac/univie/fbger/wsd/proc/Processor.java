package at.ac.univie.fbger.wsd.proc;

import at.ac.univie.fbger.wsd.model.Exemplar;
import at.ac.univie.fbger.wsd.model.ExemplarVisitor;
import com.jolbox.bonecp.BoneCP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author michael.zehender@me.com
 */
public class Processor {
	private final Logger logger = LoggerFactory.getLogger(Processor.class);

	private final ExemplarVisitor visitor;
	private final BoneCP connectionPool;

	public Processor(ExemplarVisitor visitor, BoneCP connectionPool) {
		this.visitor = visitor;
		this.connectionPool = connectionPool;
	}

	public void processReconciliation() throws SQLException {
		logger.info("performing reconciliation now");
		Connection connection = connectionPool.getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(selectStatement);
			ResultSet resultSet = statement.executeQuery();

			while (resultSet.next()) {
				Exemplar exemplar = new Exemplar();
				exemplar.setId(resultSet.getString("id"));
				exemplar.setAuthor(resultSet.getString("author"));
				exemplar.setCoAuthor(resultSet.getString("coauthor"));
				exemplar.setSeriesTitle(resultSet.getString("series_title"));
				exemplar.setTitle(resultSet.getString("title"));
				exemplar.setSubtitle(resultSet.getString("subtitle"));
				exemplar.setPublisher(resultSet.getString("publisher"));
				exemplar.setYearOfPublication(resultSet.getString("year_of_publication"));
				exemplar.setEndnoteId(resultSet.getString("endnote_id"));
				exemplar.setEndnoteAuthor(resultSet.getString("endnote_author"));
				exemplar.setEndnoteTitle(resultSet.getString("endnote_title"));
				exemplar.setEndnotePublisher(resultSet.getString("endnote_publisher"));
				exemplar.setEndnoteYearOfPublication(resultSet.getString("endnote_year_of_publication"));
				exemplar.setSeriesAcNumber(resultSet.getString("series_ac_number"));
				exemplar.setAcNumber(resultSet.getString("ac_number"));
				exemplar.setLibrary(resultSet.getString("library"));
				exemplar.setCurrentLocation(resultSet.getString("current_location"));
				exemplar.setSignature(resultSet.getString("signature"));
				exemplar.setInventoryNumber(resultSet.getString("inventory_number"));
				exemplar.setAmNumber(resultSet.getString("am_number"));
				exemplar.setOriginalLocation(resultSet.getString("original_location"));
				exemplar.setAbstract(resultSet.getString("abstract"));
				exemplar.setUrls(resultSet.getString("urls"));

				visitor.visit(exemplar);
			}

			resultSet.close();
			statement.close();
			logger.info("reconciliation ended successfully");
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// ignore
			}
		}
	}

	// INJECTED
	private String selectStatement;

	public void setSelectStatement(String selectStatement) {
		this.selectStatement = selectStatement;
	}
}
