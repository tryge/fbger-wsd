package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.impl.utils.Generator;
import at.ac.univie.fbger.wsd.impl.utils.GeneratorFunc;
import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import at.ac.univie.fbger.wsd.model.EndnoteExemplarVisitor;
import com.google.common.collect.Iterables;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Stack;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteExtractor {
	private static final String validationFeature = "http://xml.org/sax/features/validation";

	private final EndnoteExemplarVisitor visitor;

	public EndnoteExtractor(EndnoteExemplarVisitor visitor) {
		this.visitor = visitor;
	}

	public void read(InputStream stream) {
		try {
			XMLReader reader = XMLReaderFactory.createXMLReader();
			InputSource source = new InputSource(stream);

			reader.setFeature(validationFeature, false);
			reader.setContentHandler(new EndnoteContentHandler());
			reader.parse(source);
		} catch (SAXException e) {
			throw new RuntimeException("Couldn't parse the input file");
		} catch (IOException e) {
			throw new RuntimeException("There was an exception reading the file", e);
		}
	}

	private EndnoteExemplar currentExemplar;
	private DataField currentField;
	private DataField actualField;

	private interface ElementHandler {
		void start();
		void end();

		boolean check(String element);
	}

	private abstract class DataField implements ElementHandler {
		private final String element;
		protected StringBuilder builder;

		public DataField(String element) {
			this.element = element;
		}

		@Override
		public void start() {
			actualField = this;
			builder = new StringBuilder();
		}

		@Override
		public boolean check(String element) {
			return this.element.equals(element);
		}

		public void appendValue(String value) {
			builder.append(value);
		}
	}

	private class EndnoteContentHandler extends XMLFilterImpl {
		private Stack<ElementHandler> handlerStack = new Stack<ElementHandler>();

		@Override
		public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
			try {
				Iterable<ElementHandler> handlers = handlers(localName);
				ElementHandler handler = Iterables.find(handlers, notNull());
				handler.start();

				handlerStack.push(handler);
			} catch(NoSuchElementException e) {
				// then it's not interesting
			}
		}

		@SuppressWarnings("unchecked")
		private Iterable<ElementHandler> handlers(String element) {
			return new Generator<ElementHandler>(newArrayList(
				handlerFunc(styleHandler, element),
				handlerFunc(recordHandler, element),
				handlerFunc(idHandler, element),
				handlerFunc(authorHandler, element),
				handlerFunc(titleHandler, element),
				handlerFunc(originalLocationHandler, element),
				handlerFunc(abstractHandler, element),
				handlerFunc(publisherHandler, element),
				handlerFunc(yearOfPublicationHandler, element),
				handlerFunc(urlHandler, element)
			));
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			if(!handlerStack.isEmpty()) {
				ElementHandler handler = handlerStack.peek();

				if (handler.check(localName)) {
					handler.end();
					handlerStack.pop();
				}
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			if (currentField != null) {
				currentField.appendValue(new String(ch, start, length));
			}
		}
	}

	private GeneratorFunc<ElementHandler> handlerFunc(final ElementHandler handler, final String element) {
		return new GeneratorFunc<ElementHandler>() {
			@Override
			public ElementHandler generate() {
				if (handler.check(element)) {
					return handler;
				}
				return null;
			}
		};
	}

	//<editor-fold desc="recordHandler">
	private ElementHandler recordHandler = new ElementHandler() {
		@Override
		public void start() {
			currentExemplar = new EndnoteExemplar();
		}

		@Override
		public void end() {
			visitor.visit(currentExemplar);
			currentExemplar = null;
		}

		@Override
		public boolean check(String element) {
			return element.equals("record");
		}
	};
	//</editor-fold>
	//<editor-fold desc="idHandler">
	private ElementHandler idHandler = new DataField("rec-number") {
		@Override
		public void start() {
			super.start();
			currentField = this;
		}

		@Override
		public void end() {
			currentField = null;
			currentExemplar.setId(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="authorHandler">
	private ElementHandler authorHandler = new DataField("author") {
		@Override
		public void end() {
			currentExemplar.addAuthor(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="titleHandler">
	private ElementHandler titleHandler = new DataField("title") {
		@Override
		public void end() {
			String title = builder.toString();
			String[] titles = title.split("\r");
			currentExemplar.setTitle(titles[0].trim());

			StringBuilder subtitlesBuilder = new StringBuilder();
			for (int i = 1; i < titles.length; i++) {
				if (i > 1) {
					subtitlesBuilder.append(" [\\] ");
				}
				subtitlesBuilder.append(filterWhitespace(titles[i]));
			}
			currentExemplar.setSubtitle(filterWhitespace(subtitlesBuilder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="originalLocationHandler">
	private ElementHandler originalLocationHandler = new DataField("keyword") {
		@Override
		public void end() {
			currentExemplar.setOriginalLocation(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="abstractHandler">
	private ElementHandler abstractHandler = new DataField("abstract") {
		@Override
		public void end() {
			currentExemplar.setAbstract(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="publisher">
	private ElementHandler publisherHandler = new DataField("publisher") {
		@Override
		public void end() {
			currentExemplar.setPublisher(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="publisher">
	private ElementHandler yearOfPublicationHandler = new DataField("year") {
		@Override
		public void end() {
			currentExemplar.setYearOfPublication(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="publisher">
	private ElementHandler urlHandler = new DataField("url") {
		@Override
		public void start() {
			super.start();
			currentField = this;
		}

		@Override
		public void end() {
			currentField = null;
			currentExemplar.addUrl(filterWhitespace(builder.toString()));
		}
	};
	//</editor-fold>
	//<editor-fold desc="styleHandler">
	private ElementHandler styleHandler = new DataField("style") {
		@Override
		public void start() {
			currentField = this;
		}

		@Override
		public void end() {
			currentField = null;
		}

		@Override
		public void appendValue(String value) {
			actualField.appendValue(value);
		}
	};
	//</editor-fold>

	private String filterWhitespace(String value) {
		return value.replaceAll("[\n\r\t ]+", " ").trim();
	}
}
