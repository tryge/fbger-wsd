package at.ac.univie.fbger.wsd.proc;

import at.ac.univie.fbger.wsd.model.Exemplar;
import at.ac.univie.fbger.wsd.model.ExemplarVisitor;
import com.google.common.eventbus.EventBus;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * @author michael.zehender@me.com
 */
public class DeleteExemplarProcessor implements ExemplarVisitor {
	private final BoneCP connectionPool;
	private final EventBus bus;

	public DeleteExemplarProcessor(BoneCP connectionPool, EventBus bus) {
		this.connectionPool = connectionPool;
		this.bus = bus;
	}

	@Override
	public void visit(final Exemplar exemplar) {
		try {
			Connection connection = connectionPool.getConnection();
			PreparedStatement statement = null;
			try {
				statement = connection.prepareStatement(deleteSql);
				statement.setString(1, exemplar.getId());
				statement.executeUpdate();
			} finally {
				if (statement != null) {
					statement.close();
				}
				connection.close();
			}
		} catch (SQLException e) {
			bus.post(e);
		}
	}

	// injected
	private String deleteSql;

	public void setDeleteSql(String deleteSql) {
		this.deleteSql = deleteSql;
	}
}
