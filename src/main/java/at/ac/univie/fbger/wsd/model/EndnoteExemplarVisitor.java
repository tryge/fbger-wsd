package at.ac.univie.fbger.wsd.model;

/**
 * @author michael.zehender@me.com
 */
public interface EndnoteExemplarVisitor {
	void visit(EndnoteExemplar capture);
}
