package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import at.ac.univie.fbger.wsd.model.EndnoteExemplarVisitor;
import com.google.common.eventbus.EventBus;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteImporter implements EndnoteExemplarVisitor {
	private final Executor executor;
	private final EndnoteRepository repository;
	private final EventBus bus;
	private final List<EndnoteImportFilter> filters = new ArrayList<EndnoteImportFilter>();

	private int tasks = 0;

	public EndnoteImporter(Executor executor, EndnoteRepository repository, EventBus bus) {
		this.executor = executor;
		this.repository = repository;
		this.bus = bus;
	}

	public void addFilter(EndnoteImportFilter filter) {
		filters.add(filter);
	}

	@Override
	public synchronized void visit(final EndnoteExemplar exemplar) {
		tasks++;
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					if (filterApply(exemplar)) {
						repository.store(exemplar);
					}
				} catch (SQLException e) {
					bus.post(e);
				} finally {
					synchronized (EndnoteImporter.this) {
						tasks--;
						EndnoteImporter.this.notifyAll();
					}
				}
			}
		});
	}

	private boolean filterApply(EndnoteExemplar exemplar) {
		for (EndnoteImportFilter filter : filters) {
			if (!filter.applies(exemplar)) {
				return false;
			}
		}
		return true;
	}

	public synchronized void awaitFinished() throws InterruptedException {
		while (tasks > 0) {
			wait();
		}
	}
}
