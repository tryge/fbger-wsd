package at.ac.univie.fbger.wsd.impl;

import at.ac.univie.fbger.wsd.ReconciliationRule;
import at.ac.univie.fbger.wsd.ReconciliationRuleFactory;
import at.ac.univie.fbger.wsd.impl.rules.BeginsWith;
import at.ac.univie.fbger.wsd.impl.rules.ExactMatch;
import at.ac.univie.fbger.wsd.impl.utils.Generator;
import at.ac.univie.fbger.wsd.impl.utils.GeneratorFunc;
import com.google.common.collect.Iterables;

import java.util.NoSuchElementException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @author michael.zehender@me.com
 */
public class DefaultReconciliationRuleFactory implements ReconciliationRuleFactory {
	@Override
	@SuppressWarnings("unchecked")
	public ReconciliationRule create(String ruleSpec) {
		Iterable<ReconciliationRule> rules = new Generator<ReconciliationRule>(newArrayList(
			exactMatch(ruleSpec),
			beginsWithMatch(ruleSpec)
		));

		try {
			return Iterables.find(rules, notNull());
		} catch (NoSuchElementException e) {
			throw new IllegalArgumentException("Invalid Rule");
		}
	}

	//<editor-fold desc="Exact Match">
	private final Pattern exactMatchPattern = Pattern.compile("^\\s*([^\\d\\W]\\w*\\.[^\\d\\W]\\w*)\\s*=\\s*([^\\d\\W]\\w*\\.[^\\d\\W]\\w*)\\s*$");

	private GeneratorFunc<ReconciliationRule> exactMatch(final String ruleSpec) {
		return new GeneratorFunc<ReconciliationRule>() {
			@Override
			public ReconciliationRule generate() {
				Matcher matcher = exactMatchPattern.matcher(ruleSpec);
				if (matcher.matches()) {
					String lhs = matcher.group(1);
					String rhs = matcher.group(2);

					return new ExactMatch(lhs, rhs);
				}
				return null;
			}
		};
	}
	//</editor-fold>

	//<editor-fold desc="BeginsWith">
	private final Pattern beginsWithPattern = Pattern.compile("\\s*([^\\d\\W]\\w*\\.[^\\d\\W]\\w*)\\s*beginsWith\\s*([^\\d\\W]\\w*\\.[^\\d\\W]\\w*)\\s*$");

	private GeneratorFunc<ReconciliationRule> beginsWithMatch(final String ruleSpec) {
		return new GeneratorFunc<ReconciliationRule>() {
			@Override
			public ReconciliationRule generate() {
				Matcher matcher = beginsWithPattern.matcher(ruleSpec);
				if (matcher.matches()) {
					String lhs = matcher.group(1);
					String rhs = matcher.group(2);

					return new BeginsWith(lhs, rhs);
				}
				return null;
			}
		};
	}
	//</editor-fold>
}
