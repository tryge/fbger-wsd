package at.ac.univie.fbger.wsd;

import at.ac.univie.fbger.wsd.exp.CsvExporter;
import at.ac.univie.fbger.wsd.exp.TextExporter;
import at.ac.univie.fbger.wsd.imp.*;
import at.ac.univie.fbger.wsd.model.ExemplarVisitors;
import at.ac.univie.fbger.wsd.proc.DeleteExemplarProcessor;
import at.ac.univie.fbger.wsd.proc.Processor;
import at.ac.univie.fbger.wsd.workflow.*;
import com.google.common.eventbus.EventBus;
import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

import java.io.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author michael.zehender@me.com
 */
public class Main {
	public static void usage() {
		System.out.println("Usage: java at.ac.univie.fbger.wsd.Main <wsd-file> <endnote-file> [<wsd-blacklist> [<endnote-blacklist>]]");
		System.exit(1);
	}

	public static void main(String[] args) throws IOException, ClassNotFoundException, SQLException {
		if (args.length < 2 || args.length > 4) {
			usage();
		}

		File wsdFile = new File(args[0]);
		File endnoteFile = new File(args[1]);

		if (!wsdFile.exists()) {
			System.out.println("WSD File doesn't exist: " + wsdFile.getAbsolutePath());
			System.exit(2);
		}
		if (!endnoteFile.exists()) {
			System.out.println("Endnote File doesn't exist: " + endnoteFile.getAbsolutePath());
			System.exit(3);
		}

		File wsdBlacklistFile = null;
		if (args.length >= 3) {
			wsdBlacklistFile = new File(args[2]);
			if (!wsdBlacklistFile.exists()) {
				System.out.println("WSD Blacklist file doesn't exist: " + wsdBlacklistFile.getAbsolutePath());
			}
		}

		File endnoteBlacklistFile = null;
		if (args.length == 4) {
			endnoteBlacklistFile = new File(args[3]);
			if (!endnoteBlacklistFile.exists()) {
				System.out.println("Endnote Blacklist file doesn't exist: " + endnoteBlacklistFile.getAbsolutePath());
			}
		}

		BoneCP connectionPool = initConnectionPool();

		createTables(connectionPool);

		// could be done with spring... should if it gets bigger
		ExecutorService executor = Executors.newFixedThreadPool(8);
		EventBus bus = new EventBus();

		EndnoteRepository endnoteRepository = new EndnoteRepository(connectionPool);
		endnoteRepository.setInsertSQL(ENDNOTE_INSERT);
		EndnoteImporter endnoteImporter = new EndnoteImporter(executor, endnoteRepository, bus);

		if (endnoteBlacklistFile != null) {
			endnoteImporter.addFilter(
				createEndnoteBlacklistFilter(endnoteBlacklistFile)
			);
		}

		EndnoteExtractor endnoteExtractor = new EndnoteExtractor(endnoteImporter);
		EndnoteImportWorkflow endnoteWorkflow = new EndnoteImportWorkflow(executor, endnoteExtractor, endnoteImporter, bus);

		WsdRepository wsdRepository = new WsdRepository(connectionPool);
		wsdRepository.setInsertSQL(WSD_INSERT);
		WsdImporter wsdImporter = new WsdImporter(executor, wsdRepository, bus);
		wsdImporter.addFilter(new WsdLibraryFilter("FB Germanistik"));
		wsdImporter.addFilter(new WsdSignatureFilter("WSD"));

		if (wsdBlacklistFile != null) {
			wsdImporter.addFilter(
				createWsdBlacklistFilter(wsdBlacklistFile)
			);
		}

		WsdExtractor wsdExtractor = new WsdExtractor(wsdImporter);
		WsdImportWorkflow wsdWorkflow = new WsdImportWorkflow(executor, wsdExtractor, wsdImporter, bus);

		DeleteExemplarProcessor deleter = new DeleteExemplarProcessor(connectionPool, bus);
		deleter.setDeleteSql(DELETE);

		WriteUnmatchedExemplarsWorkflow shutdownWorkflow = new WriteUnmatchedExemplarsWorkflow(connectionPool, new File("unmatched-exemplars.txt"));
		shutdownWorkflow.setSelectSql("SELECT signature FROM wsd order by signature DESC");

		MainWorkflow mainWorkflow = new MainWorkflow(executor, bus);
		mainWorkflow.setEndnoteInputStream(new FileInputStream(endnoteFile));
		mainWorkflow.setEndnoteWorkflow(endnoteWorkflow);
		mainWorkflow.setWsdInputStream(new FileInputStream(wsdFile));
		mainWorkflow.setWsdWorkflow(wsdWorkflow);
		mainWorkflow.setShutdownWorkflow(shutdownWorkflow);
		mainWorkflow.addProcessWorkflow(createExactSeriesWorkflow(connectionPool, executor, bus, deleter));
		mainWorkflow.addProcessWorkflow(createApproxSeriesWorkflow(connectionPool, executor, bus, deleter));
		mainWorkflow.addProcessWorkflow(createBeginsWithSeriesWorkflow(connectionPool, executor, bus, deleter));
		mainWorkflow.addProcessWorkflow(createExactWorkflow(connectionPool, executor, bus, deleter));
		mainWorkflow.addProcessWorkflow(createApproxWorkflow(connectionPool, executor, bus, deleter));
		mainWorkflow.addProcessWorkflow(createBeginsWithWorkflow(connectionPool, executor, bus, deleter));
		mainWorkflow.execute();
	}

	private static EndnoteImportFilter createEndnoteBlacklistFilter(File endnoteBlacklistFile) throws IOException {
		EndnoteBlacklistFilter filter = new EndnoteBlacklistFilter();
		filter.addToBlacklist(
			readBlacklist(endnoteBlacklistFile)
		);

		return filter;
	}

	private static WsdImportFilter createWsdBlacklistFilter(File wsdBlacklistFile) throws IOException {
		WsdBlacklistFilter filter = new WsdBlacklistFilter();
		filter.addToBlacklist(
			readBlacklist(wsdBlacklistFile)
		);

		return filter;
	}

	private static List<String> readBlacklist(File file) throws IOException {
		List<String> blacklist = new LinkedList<String>();

		BufferedReader reader = new BufferedReader(new FileReader(file));
		for (;;) {
			String line = reader.readLine();

			if (line == null) {
				break;
			}
			line = line.trim();

			if (!line.equals("")) {
				blacklist.add(line);
			}
		}
		reader.close();

		return blacklist;
	}

	private static BoneCP initConnectionPool() throws ClassNotFoundException, SQLException {
		Class.forName("org.hsqldb.jdbc.JDBCDriver" );

		BoneCPConfig config = new BoneCPConfig();
		config.setJdbcUrl("jdbc:hsqldb:mem:reconciliation");
		config.setUsername("sa");
		config.setPassword("");
		config.setMinConnectionsPerPartition(4);
		config.setMaxConnectionsPerPartition(8);
		config.setPartitionCount(1);

		return new BoneCP(config);
	}

	private static void createTables(BoneCP connectionPool) throws SQLException {
		Connection connection = connectionPool.getConnection();

		Statement statement = connection.createStatement();
		statement.execute(WSD_CREATE);
		statement.close();

		statement = connection.createStatement();
		statement.execute(ENDNOTE_CREATE);
		statement.close();

		connection.close();
	}

	private static ProcessWorkflow createExactWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter) {
		return createWorkflow(
			connectionPool,
			executor,
			bus,
			deleter,
			"reconciliation-exact.txt",
			"reconciliation-exact.csv",
			SELECT
		);
	}

	private static ProcessWorkflow createExactSeriesWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter) {
		return createWorkflow(
			connectionPool,
			executor,
			bus,
			deleter,
			"reconciliation-series-exact.txt",
			"reconciliation-series-exact.csv",
			SELECT_SERIES
		);
	}

	private static ProcessWorkflow createApproxWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter) {
		return createWorkflow(
			connectionPool,
			executor,
			bus,
			deleter,
			"reconciliation-approx.txt",
			"reconciliation-approx.csv",
			SELECT_A
		);
	}

	private static ProcessWorkflow createApproxSeriesWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter) {
		return createWorkflow(
			connectionPool,
			executor,
			bus,
			deleter,
			"reconciliation-series-approx.txt",
			"reconciliation-series-approx.csv",
			SELECT_SERIES_A
		);
	}

	private static ProcessWorkflow createBeginsWithWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter) {
		return createWorkflow(
			connectionPool,
			executor,
			bus,
			deleter,
			"reconciliation-beginsWith.txt",
			"reconciliation-beginsWith.csv",
			SELECT_BEGINS_WITH
		);
	}

	private static ProcessWorkflow createBeginsWithSeriesWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter) {
		return createWorkflow(
			connectionPool,
			executor,
			bus,
			deleter,
			"reconciliation-series-beginsWith.txt",
			"reconciliation-series-beginsWith.csv",
			SELECT_SERIES_BEGINS_WITH
		);
	}

	private static ProcessWorkflow createWorkflow(BoneCP connectionPool, ExecutorService executor, EventBus bus, DeleteExemplarProcessor deleter, String textFile, String csvFile, String selectStatement) {
		TextExporter textExporterBeginsWith = new TextExporter(bus, new File(textFile));
		CsvExporter csvExporterBeginsWith = new CsvExporter(bus, new File(csvFile), "$");

		ExemplarVisitors visitors = new ExemplarVisitors();
		visitors.add(textExporterBeginsWith);
		visitors.add(csvExporterBeginsWith);
		visitors.add(deleter);

		Processor processor = new Processor(visitors, connectionPool);
		processor.setSelectStatement(selectStatement);
		ProcessWorkflow processWorkflow = new ProcessWorkflow(executor, bus);
		processWorkflow.setProcessor(processor);
		return processWorkflow;
	}

	public static final String WSD_CREATE = "CREATE TABLE wsd(id VARCHAR(1024) PRIMARY KEY, author VARCHAR(4096), coauthor VARCHAR(4096), series_title VARCHAR(4096), series_title_a VARCHAR(4096), title VARCHAR(4096), title_a VARCHAR(4096), subtitle VARCHAR(4096), publisher VARCHAR(4096), year_of_publication VARCHAR(4096), series_ac_number VARCHAR(4096), ac_number VARCHAR(4096), library VARCHAR(4096), located_at VARCHAR(4096), signature VARCHAR(4096), inventory_number VARCHAR(4096), am_number VARCHAR(4096));";
	public static final String WSD_INSERT = "INSERT INTO wsd (id, ac_number, am_number, author, coauthor, inventory_number, library, located_at, publisher, series_ac_number, series_title, series_title_a, signature, subtitle, title, title_a, year_of_publication) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";
	public static final String ENDNOTE_CREATE = "CREATE TABLE endnote(author VARCHAR(4096), title VARCHAR(4096), title_a VARCHAR(4096), subtitle VARCHAR(4096), publisher VARCHAR(4096), year_of_publication VARCHAR(4096), original_location VARCHAR(4096), abstract VARCHAR(4096), urls VARCHAR(4096), id VARCHAR(4096));";
	public static final String ENDNOTE_INSERT = "INSERT INTO endnote(abstract, author, original_location, publisher, subtitle, title, title_a, urls, year_of_publication, id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?);";

	public static final String DELETE = "DELETE FROM wsd WHERE id = ?;";

	public static final String SELECT = "SELECT wsd.id, wsd.series_ac_number, wsd.ac_number, wsd.am_number, wsd.author, wsd.coauthor, wsd.publisher, wsd.year_of_publication, wsd.inventory_number, wsd.library, wsd.located_at as current_location, wsd.signature, wsd.subtitle, wsd.series_title, wsd.title, endnote.id as endnote_id, endnote.author as endnote_author, CONCAT(CONCAT(endnote.title, ' [\\] '), endnote.subtitle) as endnote_title, endnote.publisher as endnote_publisher, endnote.year_of_publication as endnote_year_of_publication, endnote.original_location, endnote.abstract, endnote.urls FROM wsd, endnote WHERE endnote.title = wsd.title ORDER BY urls DESC, wsd.signature DESC;";
	public static final String SELECT_A = "SELECT wsd.id, wsd.series_ac_number, wsd.ac_number, wsd.am_number, wsd.author, wsd.coauthor, wsd.publisher, wsd.year_of_publication, wsd.inventory_number, wsd.library, wsd.located_at as current_location, wsd.signature, wsd.subtitle, wsd.series_title, wsd.title, endnote.id as endnote_id, endnote.author as endnote_author, CONCAT(CONCAT(endnote.title, ' [\\] '), endnote.subtitle) as endnote_title, endnote.publisher as endnote_publisher, endnote.year_of_publication as endnote_year_of_publication, endnote.original_location, endnote.abstract, endnote.urls FROM wsd, endnote WHERE endnote.title_a = wsd.title_a ORDER BY urls DESC, wsd.signature DESC;";
	public static final String SELECT_BEGINS_WITH = "SELECT wsd.id, wsd.series_ac_number, wsd.ac_number, wsd.am_number, wsd.author, wsd.coauthor, wsd.publisher, wsd.year_of_publication, wsd.inventory_number, wsd.library, wsd.located_at as current_location, wsd.signature, wsd.subtitle, wsd.series_title, wsd.title, endnote.id as endnote_id, endnote.author as endnote_author, CONCAT(CONCAT(endnote.title, ' [\\] '), endnote.subtitle) as endnote_title, endnote.publisher as endnote_publisher, endnote.year_of_publication as endnote_year_of_publication, endnote.original_location, endnote.abstract, endnote.urls FROM wsd, endnote WHERE endnote.title_a LIKE CONCAT(wsd.title_a, '%') ORDER BY urls DESC, wsd.signature DESC;";

	public static final String SELECT_SERIES = "SELECT wsd.id, wsd.series_ac_number, wsd.ac_number, wsd.am_number, wsd.author, wsd.coauthor, wsd.publisher, wsd.year_of_publication, wsd.inventory_number, wsd.library, wsd.located_at as current_location, wsd.signature, wsd.subtitle, wsd.series_title, wsd.title, endnote.id as endnote_id, endnote.author as endnote_author, CONCAT(CONCAT(endnote.title, ' [\\] '), endnote.subtitle) as endnote_title, endnote.publisher as endnote_publisher, endnote.year_of_publication as endnote_year_of_publication, endnote.original_location, endnote.abstract, endnote.urls FROM wsd, endnote WHERE endnote.title = wsd.series_title ORDER BY urls DESC, wsd.signature DESC;";
	public static final String SELECT_SERIES_A = "SELECT wsd.id, wsd.series_ac_number, wsd.ac_number, wsd.am_number, wsd.author, wsd.coauthor, wsd.publisher, wsd.year_of_publication, wsd.inventory_number, wsd.library, wsd.located_at as current_location, wsd.signature, wsd.subtitle, wsd.series_title, wsd.title, endnote.id as endnote_id, endnote.author as endnote_author, CONCAT(CONCAT(endnote.title, ' [\\] '), endnote.subtitle) as endnote_title, endnote.publisher as endnote_publisher, endnote.year_of_publication as endnote_year_of_publication, endnote.original_location, endnote.abstract, endnote.urls FROM wsd, endnote WHERE endnote.title_a = wsd.series_title_a ORDER BY urls DESC, wsd.signature DESC;";
	public static final String SELECT_SERIES_BEGINS_WITH = "SELECT wsd.id, wsd.series_ac_number, wsd.ac_number, wsd.am_number, wsd.author, wsd.coauthor, wsd.publisher, wsd.year_of_publication, wsd.inventory_number, wsd.library, wsd.located_at as current_location, wsd.signature, wsd.subtitle, wsd.series_title, wsd.title, endnote.id as endnote_id, endnote.author as endnote_author, CONCAT(CONCAT(endnote.title, ' [\\] '), endnote.subtitle) as endnote_title, endnote.publisher as endnote_publisher, endnote.year_of_publication as endnote_year_of_publication, endnote.original_location, endnote.abstract, endnote.urls FROM wsd, endnote WHERE wsd.series_title_a is not NULL AND endnote.title_a LIKE CONCAT(wsd.series_title_a, '%') ORDER BY urls DESC, wsd.signature DESC;";
}
