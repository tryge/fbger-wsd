package at.ac.univie.fbger.wsd.model;

/**
 * @author michael.zehender@me.com
 */
public interface WsdExemplarVisitor {
	void visit(WsdExemplar exemplar);
}
