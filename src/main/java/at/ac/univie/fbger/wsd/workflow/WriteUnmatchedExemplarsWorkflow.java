package at.ac.univie.fbger.wsd.workflow;

import com.jolbox.bonecp.BoneCP;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * @author michael.zehender@me.com
 */
public class WriteUnmatchedExemplarsWorkflow implements Workflow {
	private final BoneCP connectionPool;
	private final File outputFile;

	public WriteUnmatchedExemplarsWorkflow(BoneCP connectionPool, File outputFile) {
		this.connectionPool = connectionPool;
		this.outputFile = outputFile;
	}

	@Override
	public void execute() {
		try {
			Connection connection = connectionPool.getConnection();

			PreparedStatement statement = connection.prepareStatement(selectSql);
			ResultSet resultSet = statement.executeQuery();

			PrintStream out = new PrintStream(new FileOutputStream(outputFile));
			while (resultSet.next()) {
				out.println(value(resultSet.getString("signature")));
			}
			out.close();
			resultSet.close();
			statement.close();
			connection.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private String value(String orig) {
		if (orig == null) {
			return "";
		}
		return orig.trim();
	}

	// INJECTED
	private String selectSql;

	public void setSelectSql(String selectSql) {
		this.selectSql = selectSql;
	}
}
