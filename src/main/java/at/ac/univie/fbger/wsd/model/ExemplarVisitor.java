package at.ac.univie.fbger.wsd.model;

/**
 * The visitor is used to visit all the exemplars of a rule's output.
 *
 * @author michael.zehender@me.com
 */
public interface ExemplarVisitor {
	void visit(Exemplar exemplar);
}
