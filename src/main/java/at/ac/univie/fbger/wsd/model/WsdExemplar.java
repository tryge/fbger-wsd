package at.ac.univie.fbger.wsd.model;

/**
 * @author michael.zehender@me.com
 */
public class WsdExemplar {
	//<editor-fold desc="author">
	private String author;

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}
	//</editor-fold>
	//<editor-fold desc="coAuthor">
	private String coAuthor;

	public String getCoAuthor() {
		return coAuthor;
	}

	public void setCoAuthor(String coAuthor) {
		this.coAuthor = coAuthor;
	}
	//</editor-fold>
	//<editor-fold desc="seriesTitle">
	private String seriesTitle;

	public String getSeriesTitle() {
		return seriesTitle;
	}

	public void setSeriesTitle(String seriesTitle) {
		this.seriesTitle = seriesTitle;
	}
	//</editor-fold>
	//<editor-fold desc="title">
	private String title;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	//</editor-fold>
	//<editor-fold desc="subtitle">
	private String subtitle;

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	//</editor-fold>
	//<editor-fold desc="seriesAcNumber">
	private String seriesAcNumber;

	public String getSeriesAcNumber() {
		return seriesAcNumber;
	}

	public void setSeriesAcNumber(String seriesAcNumber) {
		this.seriesAcNumber = seriesAcNumber;
	}
	//</editor-fold>
	//<editor-fold desc="acNumber">
	private String acNumber;

	public String getAcNumber() {
		return acNumber;
	}

	public void setAcNumber(String acNumber) {
		this.acNumber = acNumber;
	}
	//</editor-fold>
	//<editor-fold desc="library">
	private String library;

	public String getLibrary() {
		return library;
	}

	public void setLibrary(String library) {
		this.library = library;
	}
	//</editor-fold>
	//<editor-fold desc="locatedAt">
	private String locatedAt;

	public String getLocatedAt() {
		return locatedAt;
	}

	public void setLocatedAt(String locatedAt) {
		this.locatedAt = locatedAt;
	}
	//</editor-fold>
	//<editor-fold desc="signature">
	private String signature;
	
	public String getSignature() {
		return signature;
	}
	
	public void setSignature(String signature) {
		this.signature = signature;
	}
	//</editor-fold>
	//<editor-fold desc="inventoryNumber">
	private String inventoryNumber;

	public String getInventoryNumber() {
		return inventoryNumber;
	}

	public void setInventoryNumber(String inventoryNumber) {
		this.inventoryNumber = inventoryNumber;
	}
	//</editor-fold>
	//<editor-fold desc="amNumber">
	private String amNumber;
	
	public String getAmNumber() {
		return amNumber;
	}
	
	public void setAmNumber(String amNumber) {
		this.amNumber = amNumber;
	}
	//</editor-fold>
	//<editor-fold desc="publisher">
	private String publisher;

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	//</editor-fold>
	//<editor-fold desc="yearOfPublication">
	private String yearOfPublication;

	public String getYearOfPublication() {
		return yearOfPublication;
	}

	public void setYearOfPublication(String yearOfPublication) {
		this.yearOfPublication = yearOfPublication;
	}
	//</editor-fold>

	public WsdExemplar anotherExemplar() {
		WsdExemplar another = new WsdExemplar();
		another.setAuthor(author);
		another.setCoAuthor(coAuthor);
		another.setSeriesTitle(seriesTitle);
		another.setTitle(title);
		another.setSubtitle(subtitle);
		another.setSeriesAcNumber(seriesAcNumber);
		another.setAcNumber(acNumber);
		another.setPublisher(publisher);
		another.setYearOfPublication(yearOfPublication);

		return another;
	}

	public String getTitleApproximated() {
		if (title == null)
			return null;
		return title.replaceAll("[<>]","").replaceAll("[ \t\r\n,\\-\":'?\\.!]+", " ");
	}

	public String getSeriesTitleApproximated() {
		if (seriesTitle == null)
			return null;
		return seriesTitle.replaceAll("[<>]","").replaceAll("[ \t\r\n,\\-\":'?\\.!]+", " ");
	}
}
