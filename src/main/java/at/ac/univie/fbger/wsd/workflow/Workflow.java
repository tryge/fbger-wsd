package at.ac.univie.fbger.wsd.workflow;

/**
 * @author michael.zehender@me.com
 */
public interface Workflow {
	void execute();
}
