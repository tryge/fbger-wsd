package at.ac.univie.fbger.wsd.model;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author michael.zehender@me.com
 */
public class EndnoteExemplar {
	//<editor-fold desc="id">
	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	//</editor-fold>
	//<editor-fold desc="author">
	private List<String> author = newArrayList();

	public List<String> getAuthors() {
		return author;
	}

	public void addAuthor(String author) {
		this.author.add(author);
	}
	//</editor-fold>
	//<editor-fold desc="title">
	private String title;
	
	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	//</editor-fold>
	//<editor-fold desc="subtitle">
	private String subtitle;

	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	//</editor-fold>
	//<editor-fold desc="originalLocation">
	private String originalLocation;

	public String getOriginalLocation() {
		return originalLocation;
	}

	public void setOriginalLocation(String originalLocation) {
		this.originalLocation = originalLocation;
	}
	//</editor-fold>
	//<editor-fold desc="abstract">
	private String abstractProp;

	public String getAbstract() {
		return abstractProp;
	}

	public void setAbstract(String abstractProp) {
		this.abstractProp = abstractProp;
	}
	//</editor-fold>
	//<editor-fold desc="publisher">
	private String publisher;

	public String getPublisher() {
		return publisher;
	}

	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	//</editor-fold>
	//<editor-fold desc="urls">
	private List<String> urls = newArrayList();

	public List<String> getUrls() {
		return urls;
	}

	public void addUrl(String url) {
		urls.add(url);
	}
	//</editor-fold>
	//<editor-fold desc="yearOfPublication">
	private String yearOfPublication;
	
	public String getYearOfPublication() {
		return yearOfPublication;
	}
	
	public void setYearOfPublication(String yearOfPublication) {
		this.yearOfPublication = yearOfPublication;
	}
	//</editor-fold>

	public String getTitleApproximated() {
		if (title == null) {
			return null;
		}

		return title.replaceAll("[<>]","").replaceAll("[ \t\r\n,\\-\":'?\\.!]+", " ");
	}
}
