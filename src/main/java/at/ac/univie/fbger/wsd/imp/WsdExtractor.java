package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.impl.utils.Generator;
import at.ac.univie.fbger.wsd.impl.utils.GeneratorFunc;
import at.ac.univie.fbger.wsd.model.WsdExemplar;
import at.ac.univie.fbger.wsd.model.WsdExemplarVisitor;
import com.google.common.collect.Iterables;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLFilterImpl;
import org.xml.sax.helpers.XMLReaderFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.NoSuchElementException;
import java.util.Stack;

import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Lists.newArrayList;

/**
 * @author michael.zehender@me.com
 */
public class WsdExtractor {
	private static final String validationFeature = "http://xml.org/sax/features/validation";

	private final WsdExemplarVisitor visitor;

	public WsdExtractor(WsdExemplarVisitor visitor) {
		this.visitor = visitor;
	}

	public void read(InputStream stream) {
		try {
			XMLReader reader = XMLReaderFactory.createXMLReader();
			InputSource source = new InputSource(stream);

			reader.setFeature(validationFeature, false);
			reader.setContentHandler(new WsdContentHandler());
			reader.parse(source);
		} catch (SAXException e) {
			throw new RuntimeException("Couldn't parse the input file");
		} catch (IOException e) {
			throw new RuntimeException("There was an exception reading the file", e);
		}
	}

	private WsdExemplar currentExemplar;
	private DataField currentField;
	private String currentCode;

	private interface ElementHandler {
		void start();
		void end();

		boolean check(String element);
	}

	private interface DataField {
		void appendValue(String code, String value);
	}

	private class WsdContentHandler extends XMLFilterImpl {
		private Stack<ElementHandler> handlerStack = new Stack<ElementHandler>();

		@Override
		public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
			try {
				Iterable<ElementHandler> handlers = handlers(localName, atts);
				ElementHandler handler = Iterables.find(handlers, notNull());
				handler.start();

				handlerStack.push(handler);
			} catch(NoSuchElementException e) {
				// then it's not interesting
			}
		}

		@SuppressWarnings("unchecked")
		private Iterable<ElementHandler> handlers(String element, Attributes atts) {
			return new Generator<ElementHandler>(newArrayList(
				subfieldHandler(element, atts),
				fieldHandler(authorHandler, "100", element, atts),
				indicatedFieldHandler(seriesTitleHandler, "331", "2", element, atts),
				fieldHandler(titleHandler, "331", element, atts),
				fieldHandler(subtitleHandler, "335", element, atts),
				indicatedFieldHandler(seriesAcNumberHandler, "001", "2", element, atts),
				fieldHandler(acNumberHandler, "001", element, atts),
				fieldHandler(z30Handler, "Z30", element, atts),
				fieldHandler(publisherHandler, "412", element, atts),
				fieldHandler(yearOfPublicationHandler, "425", element, atts),
				recordHandler(element),
				fieldHandler(coAuthorHandler, "104", element, atts)
			));
		}

		@Override
		public void endElement(String uri, String localName, String qName) {
			if(!handlerStack.isEmpty()) {
				ElementHandler handler = handlerStack.peek();

				if (handler.check(localName)) {
					handler.end();
					handlerStack.pop();
				}
			}
		}

		@Override
		public void characters(char[] ch, int start, int length) {
			if (currentField != null) {
				currentField.appendValue(currentCode, new String(ch, start, length));
			}
		}


	}

	//<editor-fold desc="recordHandler">
	private GeneratorFunc<ElementHandler> recordHandler(final String element) {
		return new GeneratorFunc<ElementHandler>() {
			@Override
			public ElementHandler generate() {
				if (recordHandler.check(element)) {
					return recordHandler;
				}
				return null;
			}
		};
	}

	private ElementHandler recordHandler = new ElementHandler() {
		@Override
		public void start() {
			currentExemplar = new WsdExemplar();
		}

		@Override
		public void end() {
		}

		@Override
		public boolean check(String element) {
			return element.equals("record");
		}
	};
	//</editor-fold>
	//<editor-fold desc="fieldHandler">
	private GeneratorFunc<ElementHandler> fieldHandler(final FieldElementHandler handler, final String tag, final String element, final Attributes atts) {
		return new GeneratorFunc<ElementHandler>() {
			@Override
			public ElementHandler generate() {
				if (handler.check(element) && tag.equals(atts.getValue("tag"))) {
					return handler;
				}
				return null;
			}
		};
	}

	private GeneratorFunc<ElementHandler> indicatedFieldHandler(final FieldElementHandler handler, final String tag, final String indicator, final String element, final Attributes atts) {
		return new GeneratorFunc<ElementHandler>() {
			@Override
			public ElementHandler generate() {
				if (handler.check(element) && tag.equals(atts.getValue("tag")) && indicator.equals(atts.getValue("ind2"))) {
					return handler;
				}
				return null;
			}
		};
	}

	private abstract class Subfield implements ElementHandler, DataField {
		private final String[] codes;
		private StringBuilder builder;

		private Subfield(String... codes) {
			this.codes = codes;
		}

		@Override
		public void appendValue(String code, String value) {
			for (String c : codes) {
				if (c.equals(code)) {
					builder.append(value);
				}
			}
		}

		@Override
		public void start() {
			builder = new StringBuilder();
		}

		@Override
		public void end() {
			setValue(builder.toString());
		}

		public abstract void setValue(String value);

		@Override
		public boolean check(String element) {
			// unused
			return false;
		}
	}

	private class FieldElementHandler implements ElementHandler, DataField {
		private final Subfield[] fields;

		private FieldElementHandler(Subfield... fields) {
			this.fields = fields;
		}

		@Override
		public void appendValue(String code, String value) {
			for (Subfield field : fields) {
				field.appendValue(code, value);
			}
		}

		@Override
		public void start() {
			currentField = this;
			for (Subfield field : fields) {
				field.start();
			}
		}

		@Override
		public void end() {
			currentField = null;
			for (Subfield field : fields) {
				field.end();
			}
		}

		@Override
		public boolean check(String element) {
			return element.equals("datafield");
		}
	}
	//</editor-fold>
	//<editor-fold desc="authorHandler">
	private FieldElementHandler authorHandler = new FieldElementHandler(new Subfield("a", "p") {
		@Override
		public void setValue(String value) {
			currentExemplar.setAuthor(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="coAuthorHandler">
	private FieldElementHandler coAuthorHandler = new FieldElementHandler(new Subfield("a", "p") {
		@Override
		public void setValue(String value) {
			currentExemplar.setCoAuthor(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="seriesTitleHandler">
	private FieldElementHandler seriesTitleHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setSeriesTitle(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="titleHandler">
	private FieldElementHandler titleHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setTitle(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="subtitleHandler">
	private FieldElementHandler subtitleHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setSubtitle(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="seriesAcNumberHandler">
	private FieldElementHandler seriesAcNumberHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setSeriesAcNumber(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="acNumberHandler">
	private FieldElementHandler acNumberHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setAcNumber(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="publisherHandler">
	private FieldElementHandler publisherHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setPublisher(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="yearOfPublicationHandler">
	private FieldElementHandler yearOfPublicationHandler = new FieldElementHandler(new Subfield("a") {
		@Override
		public void setValue(String value) {
			currentExemplar.setYearOfPublication(value);
		}
	});
	//</editor-fold>
	//<editor-fold desc="ExemplarDaten">
	private FieldElementHandler z30Handler = new FieldElementHandler(
		new Subfield("A") {
			@Override
			public void setValue(String value) {
				currentExemplar.setLibrary(value);
			}
		},
		new Subfield("2") {
			@Override
			public void setValue(String value) {
				currentExemplar.setLocatedAt(value);
			}
		},
		new Subfield("9") {
			@Override
			public void setValue(String value) {
				currentExemplar.setSignature(value);
			}
		},
		new Subfield("6") {
			@Override
			public void setValue(String value) {
				currentExemplar.setInventoryNumber(value);
			}
		},
		new Subfield("5") {
			@Override
			public void setValue(String value) {
				currentExemplar.setAmNumber(value);
			}
		}
	) {
		@Override
		public void start() {
			currentExemplar = currentExemplar.anotherExemplar();
			super.start();
		}

		@Override
		public void end() {
			super.end();
			visitor.visit(currentExemplar);
		}
	};
	//</editor-fold>
	//<editor-fold desc="subfieldHandler">
	private GeneratorFunc<ElementHandler> subfieldHandler(final String element, final Attributes atts) {
		return new GeneratorFunc<ElementHandler>() {
			@Override
			public ElementHandler generate() {
				if (subfieldHandler.check(element)) {
					currentCode = atts.getValue("code");
					return subfieldHandler;
				}
				return null;
			}
		};
	}

	private ElementHandler subfieldHandler = new ElementHandler() {
		@Override
		public void start() {
		}

		@Override
		public void end() {
			currentCode = null;
		}

		@Override
		public boolean check(String element) {
			return "subfield".equals(element);
		}
	};
	//</editor-fold>
}
