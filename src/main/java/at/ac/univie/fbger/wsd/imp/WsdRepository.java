package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author michael.zehender@me.com
 */
public class WsdRepository extends Repository {
	private final BoneCP connectionPool;
	private final AtomicInteger idCounter = new AtomicInteger(0);

	public WsdRepository(BoneCP dataSource) {
		this.connectionPool = dataSource;
	}

	public void store(WsdExemplar exemplar) throws SQLException {
		Connection connection = connectionPool.getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(insertSQL);
			setStringValue(statement, 1, getNextId());
			setStringValue(statement, 2, exemplar.getAcNumber());
			setStringValue(statement, 3, exemplar.getAmNumber());
			setStringValue(statement, 4, exemplar.getAuthor());
			setStringValue(statement, 5, exemplar.getCoAuthor());
			setStringValue(statement, 6, exemplar.getInventoryNumber());
			setStringValue(statement, 7, exemplar.getLibrary());
			setStringValue(statement, 8, exemplar.getLocatedAt());
			setStringValue(statement, 9, exemplar.getPublisher());
			setStringValue(statement, 10, exemplar.getSeriesAcNumber());
			setStringValue(statement, 11, exemplar.getSeriesTitle());
			setStringValue(statement, 12, exemplar.getSeriesTitleApproximated());
			setStringValue(statement, 13, exemplar.getSignature());
			setStringValue(statement, 14, exemplar.getSubtitle());
			setStringValue(statement, 15, exemplar.getTitle());
			setStringValue(statement, 16, exemplar.getTitleApproximated());
			setStringValue(statement, 17, exemplar.getYearOfPublication());
			statement.executeUpdate();
			statement.close();
		} finally {
			try {
				connection.close();
			} catch (SQLException sqle) {
				// ignore
			}
		}
	}

	private String getNextId() {
		return "WSD-" + idCounter.incrementAndGet();
	}

	// INJECTED
	private String insertSQL;

	public void setInsertSQL(String insertSQL) {
		this.insertSQL = insertSQL;
	}
}
