package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;

/**
 * @author michael.zehender@me.com
 */
public interface WsdImportFilter {
	/**
	 * Checks whether this filter applies to the exemplar.
	 *
	 * @param exemplar to check
	 * @return whether this filter applies to the specified exemplar.
	 */
	boolean applies(WsdExemplar exemplar);
}
