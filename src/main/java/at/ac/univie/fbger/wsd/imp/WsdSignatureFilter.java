package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.WsdExemplar;
import com.google.common.base.Preconditions;

/**
 * @author michael.zehender@me.com
 */
public class WsdSignatureFilter implements WsdImportFilter {
	private final String signaturePrefix;

	public WsdSignatureFilter(String signaturePrefix) {
		Preconditions.checkNotNull(signaturePrefix);

		this.signaturePrefix = signaturePrefix;
	}

	@Override
	public boolean applies(WsdExemplar exemplar) {
		Preconditions.checkNotNull(exemplar);

		String signature = exemplar.getSignature();
		return signature != null && signature.startsWith(signaturePrefix);
	}
}
