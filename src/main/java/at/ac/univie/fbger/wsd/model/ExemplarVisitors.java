package at.ac.univie.fbger.wsd.model;

import java.util.ArrayList;

import static com.google.common.collect.Lists.newArrayList;

/**
 * @author michael.zehender@me.com
 */
public class ExemplarVisitors implements ExemplarVisitor {
	private final ArrayList<ExemplarVisitor> visitors = newArrayList();

	public void add(ExemplarVisitor visitor) {
		visitors.add(visitor);
	}

	@Override
	public void visit(Exemplar exemplar) {
		for (ExemplarVisitor visitor : visitors) {
			visitor.visit(exemplar);
		}
	}
}
