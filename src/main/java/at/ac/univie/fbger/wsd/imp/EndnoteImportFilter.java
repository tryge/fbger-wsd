package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;

/**
 * @author michael.zehender@me.com
 */
public interface EndnoteImportFilter {
	/**
	 * Checks whether this filter applies to the exemplar.
	 *
	 * @param exemplar to check
	 * @return whether this filter applies to the specified exemplar.
	 */
	boolean applies(EndnoteExemplar exemplar);
}
