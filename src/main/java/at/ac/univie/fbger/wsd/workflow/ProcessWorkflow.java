package at.ac.univie.fbger.wsd.workflow;

import at.ac.univie.fbger.wsd.proc.Processor;
import com.google.common.eventbus.EventBus;

import java.sql.SQLException;
import java.util.concurrent.Executor;

/**
 * @author michael.zehender@me.com
 */
public class ProcessWorkflow {
	private final Executor executor;
	private final EventBus bus;

	public ProcessWorkflow(Executor executor, EventBus bus) {
		this.executor = executor;
		this.bus = bus;
	}

	public void execute() {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					processor.processReconciliation();
					bus.post("DONE");
				} catch (SQLException e) {
					bus.post(e);
				}
			}
		});
	}

	// INJECTED
	private Processor processor;

	public void setProcessor(Processor processor) {
		this.processor = processor;
	}
}
