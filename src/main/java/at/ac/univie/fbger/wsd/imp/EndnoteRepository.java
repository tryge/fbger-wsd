package at.ac.univie.fbger.wsd.imp;

import at.ac.univie.fbger.wsd.model.EndnoteExemplar;
import com.google.common.base.Joiner;
import com.jolbox.bonecp.BoneCP;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;


/**
 * @author michael.zehender@me.com
 */
public class EndnoteRepository extends Repository {
	private final BoneCP connectionPool;

	public EndnoteRepository(BoneCP connectionPool) {
		this.connectionPool = connectionPool;
	}

	public void store(EndnoteExemplar exemplar) throws SQLException {
		Connection connection = connectionPool.getConnection();

		try {
			PreparedStatement statement = connection.prepareStatement(insertSQL);
			setStringValue(statement, 1, exemplar.getAbstract());
			setStringValue(statement, 2, Joiner.on(" ; ").join(exemplar.getAuthors()));
			setStringValue(statement, 3, exemplar.getOriginalLocation());
			setStringValue(statement, 4, exemplar.getPublisher());
			setStringValue(statement, 5, exemplar.getSubtitle());
			setStringValue(statement, 6, exemplar.getTitle());
			setStringValue(statement, 7, exemplar.getTitleApproximated());
			setStringValue(statement, 8, Joiner.on(";").join(exemplar.getUrls()));
			setStringValue(statement, 9, exemplar.getYearOfPublication());
			setStringValue(statement, 10, exemplar.getId());
			statement.executeUpdate();
			statement.close();
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				// ignore
			}
		}
	}

	// INJECTED
	private String insertSQL;

	public void setInsertSQL(String insertSQL) {
		this.insertSQL = insertSQL;
	}
}
