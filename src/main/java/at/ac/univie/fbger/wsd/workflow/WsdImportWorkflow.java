package at.ac.univie.fbger.wsd.workflow;

import at.ac.univie.fbger.wsd.imp.WsdExtractor;
import at.ac.univie.fbger.wsd.imp.WsdImporter;
import com.google.common.eventbus.EventBus;

import java.io.InputStream;
import java.util.concurrent.Executor;

/**
 * @author michael.zehender@me.com
 */
public class WsdImportWorkflow {
	private final Executor executor;
	private final WsdExtractor extractor;
	private final WsdImporter importer;
	private final EventBus bus;

	public WsdImportWorkflow(Executor executor, WsdExtractor extractor, WsdImporter importer, EventBus bus) {
		this.executor = executor;
		this.extractor = extractor;
		this.importer = importer;
		this.bus = bus;
	}

	public void execute(final InputStream is) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					extractor.read(is);
					importer.awaitFinished();
					bus.post("WSD");
				} catch (Exception e) {
					bus.post(e);
				}
			}
		});
	}
}
