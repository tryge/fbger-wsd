package at.ac.univie.fbger.wsd;

/**
 * @author michael.zehender@me.com
 */
public interface ReconciliationRuleFactory {
	ReconciliationRule create(String ruleSpec);
}
