package at.ac.univie.fbger.wsd;

import com.google.common.base.Joiner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author michael.zehender@me.com
 */
public class Reconciliation {
	private final String outputFile;
	private List<ReconciliationRule> rules;

	public Reconciliation(String outputFile, List<ReconciliationRule> rules) {
		this.outputFile = outputFile;
		this.rules = rules;
	}

	public String getOutputFile() {
		return outputFile;
	}

	public List<ReconciliationRule> getRules() {
		return Collections.unmodifiableList(rules);
	}

	public String toSQL() {
		List<String> conditions = new ArrayList<String>(rules.size());

		for (ReconciliationRule rule : rules) {
			conditions.add(rule.toSQL());
		}

		return Joiner.on(" AND ").join(conditions);
	}
}
